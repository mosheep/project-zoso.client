﻿using UnityEngine;

namespace Karen90MmoFramework.Client
{
	/// <summary>
	/// Items that can be tracked by radar
	/// </summary>
	public interface IBlip
	{
		/// <summary>
		/// Gets the blip icon
		/// </summary>
		Texture2D BlipIcon { get; }

		/// <summary>
		/// Gets the current position
		/// </summary>
		Vector3 Position { get; }
	}
}
