﻿namespace Karen90MmoFramework.Client
{
	public interface IMinimap
	{
		/// <summary>
		/// Gets the zoom percent
		/// </summary>
		float ZoomPercent { get; }
		
		/// <summary>
		/// Adds a poi
		/// </summary>
		/// <param name="poi"></param>
		void AddPOI(IBlip poi);

		/// <summary>
		/// Removes a poi
		/// </summary>
		/// <param name="poi"></param>
		void RemovePOI(IBlip poi);

		/// <summary>
		/// Updates the poi
		/// </summary>
		/// <param name="poi"></param>
		void UpdatePOI(IBlip poi);

		/// <summary>
		/// Zooms in the map
		/// </summary>
		void ZoomIn();

		/// <summary>
		/// Zooms out the map
		/// </summary>
		void ZoomOut();
	}
}
