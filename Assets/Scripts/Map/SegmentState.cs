﻿namespace Karen90MmoFramework.Client
{
	public enum SegmentState : byte
	{
		Destroyed,
		Active,
		Loading,
	}
}
