using UnityEngine;

public class FogOverTime : MonoBehaviour
{
    private enum playerState { entering, exiting, idle };

    public bool changeFog = false;
    public bool changeAmbientLight = false;
    public bool changeSunIntensity = false;

    // public variables
    public float targetFogDensity = 0.02f;
    public float targetSunLightIntensity = 0.1f;
    public Color targetAmbientColor = Color.white;

	public float speed;

	private Light sun;
    private playerState fogState;
	private float initialFogDensity;
	private float initialSunLightIntensity;
    private Color initialAmbientColor;
    private Color tempAmbientColor;
    private bool functionIdle;

	private float sunIntensityNormal;
	private float fogDensityNormal;
	private float ambiantRedNormal;
	private float ambiantGreenNormal;
	private float ambiantBlueNormal;

	private float time;
	private const float MaxResetTimer = 6.0f;

	void Awake()
    {
	    var sunObject = GameObject.FindWithTag("Sun");
		if(sunObject)
			this.sun = sunObject.GetComponent<Light>();

        fogState = playerState.idle;
		initialSunLightIntensity = this.sun.intensity;
		initialFogDensity = RenderSettings.fogDensity;
        initialAmbientColor = RenderSettings.ambientLight;
        tempAmbientColor = initialAmbientColor;
        functionIdle = true;
	}

    void Update()
    {
		if (fogState == playerState.entering && functionIdle == false)
		{
			ApplyFog(targetSunLightIntensity, targetFogDensity, targetAmbientColor);
		}
		else if (fogState == playerState.exiting && this.functionIdle)
		{
			ApplyFog(initialSunLightIntensity, initialFogDensity, initialAmbientColor);

			if (time < MaxResetTimer)
				time += Time.deltaTime;
			else
			{
				this.sun.intensity = initialSunLightIntensity;
				RenderSettings.fogDensity = initialFogDensity;
				RenderSettings.ambientLight = initialAmbientColor;
				fogState = playerState.idle;
			}
		}
    }

    void OnTriggerEnter(Collider player) 
    {
        if (player.tag == "Player" && functionIdle == true)
        {
            fogState = playerState.entering;
			calculateNormals(targetSunLightIntensity, targetFogDensity, targetAmbientColor);
			functionIdle = false;
        }
    }

    void OnTriggerExit(Collider player)
    {
        if (player.tag == "Player" && functionIdle == false)
        {
			time = 0;
            fogState = playerState.exiting;
			calculateNormals(initialSunLightIntensity, initialFogDensity, initialAmbientColor);
			functionIdle = true;
        }
    }

	void ApplyFog(float targetSunIntensity, float targetFogDensity, Color targetAmbientColor)
	{
		if (changeSunIntensity)
		{
			if ((this.sun.intensity < targetSunIntensity && sunIntensityNormal == 1) ||
				(this.sun.intensity > targetSunIntensity && sunIntensityNormal == -1))
				this.sun.intensity += (sunIntensityNormal * speed * Time.deltaTime);
		}

		if (changeFog)
		{
			if ((RenderSettings.fogDensity < targetFogDensity && fogDensityNormal == 1) ||
				(RenderSettings.fogDensity > targetFogDensity && fogDensityNormal == -1))
				RenderSettings.fogDensity += (fogDensityNormal * speed * Time.deltaTime / 30);
		}

		if (changeAmbientLight)
		{
			if ((tempAmbientColor.r < targetAmbientColor.r && ambiantRedNormal == 1) ||
				(tempAmbientColor.r > targetAmbientColor.r && ambiantRedNormal == -1))
				tempAmbientColor.r += (ambiantRedNormal * speed * Time.deltaTime);

			if ((tempAmbientColor.g < targetAmbientColor.g && ambiantGreenNormal == 1) ||
				(tempAmbientColor.g > targetAmbientColor.g && ambiantGreenNormal == -1))
				tempAmbientColor.g += (ambiantGreenNormal * speed * Time.deltaTime);

			if ((tempAmbientColor.b < targetAmbientColor.b && ambiantBlueNormal == 1) ||
				(tempAmbientColor.b > targetAmbientColor.b && ambiantBlueNormal == -1))
				tempAmbientColor.b += (ambiantBlueNormal * speed * Time.deltaTime);

			RenderSettings.ambientLight = tempAmbientColor;
		}
	}

	void calculateNormals(float targetSunIntensity, float targetFogDensity, Color targetAmbientColor)
	{
		tempAmbientColor = RenderSettings.ambientLight;

		sunIntensityNormal = (targetSunIntensity - this.sun.intensity) == 0 ? 0 : Mathf.RoundToInt((targetSunIntensity - this.sun.intensity) / Mathf.Abs((targetSunIntensity - this.sun.intensity)));
		fogDensityNormal = (targetFogDensity - RenderSettings.fogDensity) == 0 ? 0 : Mathf.RoundToInt((targetFogDensity - RenderSettings.fogDensity) / Mathf.Abs((targetFogDensity - RenderSettings.fogDensity)));
		ambiantRedNormal = (targetAmbientColor.r - RenderSettings.ambientLight.r) == 0 ? 0 : Mathf.RoundToInt((targetAmbientColor.r - RenderSettings.ambientLight.r) / Mathf.Abs((targetAmbientColor.r - RenderSettings.ambientLight.r)));
		ambiantGreenNormal = (targetAmbientColor.g - RenderSettings.ambientLight.g) == 0 ? 0 : Mathf.RoundToInt((targetAmbientColor.g - RenderSettings.ambientLight.g) / Mathf.Abs((targetAmbientColor.g - RenderSettings.ambientLight.g)));
		ambiantBlueNormal = (targetAmbientColor.b - RenderSettings.ambientLight.b) == 0 ? 0 : Mathf.RoundToInt((targetAmbientColor.b - RenderSettings.ambientLight.b) / Mathf.Abs((targetAmbientColor.b - RenderSettings.ambientLight.b)));
	}
};
