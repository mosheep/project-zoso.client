﻿using UnityEngine;

/// <summary>
/// An object which will persist through scene loads throughout the game and will be destroyed only when the game resets
/// </summary>
public class SystemPersister : MonoBehaviour
{
	void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}
	
	public void Destroy()
	{
		Destroy(gameObject);
	}
}
