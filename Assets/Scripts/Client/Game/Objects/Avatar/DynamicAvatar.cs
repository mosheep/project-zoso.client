﻿using System;

using UnityEngine;

using Karen90MmoFramework.Client.Game.Objects;
using Karen90MmoFramework.Hud;

public class DynamicAvatar : Avatar
{
	#region Constants and Fields

	//private Dynamic elementOwner;

	#endregion

	#region Avatar Methods Methods

	/// <summary>
	/// Called on Awake
	/// </summary>
	protected override void OnAwake()
	{
	}

	/// <summary>
	/// Called on Update
	/// </summary>
	protected override void OnUpdate()
	{
		if (ShowTooltip)
		{
			TooltipManager.ValidateTooltip();
		}
	}

	/// <summary>
	/// Called after setting the owner
	/// </summary>
	/// <param name="newOwner"></param>
	protected override void OnSetOwner(MmoObject newOwner)
	{
		var owner = newOwner as Dynamic;
		if (owner == null)
			throw new InvalidCastException();

		//this.elementOwner = owner;
		this.AvatarName = newOwner.Name; // this.name != this.Name; remember is GameObject.name
	}

	/// <summary>
	/// Called when destroyed
	/// </summary>
	protected override sealed void OnAvatarDestroy()
	{
		this.DestroyGameObject();
	}

	/// <summary>
	/// Destroys all created <see cref="GameObject"/>.
	/// </summary>
	protected override void DestroyGameObject()
	{
		Destroy(this.gameObject);
	}

	/// <summary>
	/// Called on mouse over
	/// </summary>
	protected override sealed void OnBeginHighlight()
	{
	}

	/// <summary>
	/// Called on mouse leave
	/// </summary>
	protected override sealed void OnEndHighlight()
	{
	}

	#endregion
}
