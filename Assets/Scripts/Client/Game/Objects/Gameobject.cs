﻿using UnityEngine;

using System;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Game.Systems;

namespace Karen90MmoFramework.Client.Game.Objects
{
	public class Gameobject : MmoObject
	{
		#region Constants and Fields

		private readonly GameObjectType goType;

		private Loot loot;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the <see cref="GameObjectType"/>.
		/// </summary>
		public GameObjectType GOType
		{
			get
			{
				return this.goType;
			}
		}

		#endregion

		#region Constructors and Destructors

		public Gameobject(MmoWorld world, MmoGuid guid, GameObjectType goType, IAvatar avatar)
			: base(world, guid, avatar)
		{
			this.goType = goType;
		}

		#endregion

		#region Game System Methods

		/// <summary>
		/// Called when the <see cref="Gameobject"/> is sapwned
		/// </summary>
		protected override void OnSpawn(Vector3 position, Vector3 rotation)
		{
			this.SetPosition(position, false);
			this.SetRotation(rotation, true);
			this.Avatar.Spawn(this.Position);
		}

		/// <summary>
		/// Called when the <see cref="Gameobject"/> is destroyed
		/// </summary>
		protected override void OnDestroy()
		{
			this.SetInteractability(false);

			if(World.Player.CurrentInteractor == this)
				World.Player.CloseInteraction();

			if(World.Player.CurrentTarget == this)
				World.Player.CloseTarget();
		}

		#endregion

		#region Object Identification

		/// <summary>
		/// Returns false
		/// </summary>
		public sealed override bool IsMerchant()
		{
			return false;
		}

		/// <summary>
		/// Returns false
		/// </summary>
		public sealed override bool IsCivilian()
		{
			return false;
		}

		/// <summary>
		/// Returns false
		/// </summary>
		public sealed override bool IsGuard()
		{
			return false;
		}

		/// <summary>
		/// Returns false
		/// </summary>
		public sealed override bool IsTrainer()
		{
			return false;
		}

		/// <summary>
		/// Returns if the <see cref="Gameobject"/> is of type <see cref="GameObjectType.Plant"/>.
		/// </summary>
		public sealed override bool IsPlant()
		{
			return this.goType == GameObjectType.Plant;
		}

		/// <summary>
		/// Returns if the <see cref="Gameobject"/> is of type <see cref="GameObjectType.Vein"/>.
		/// </summary>
		public sealed override bool IsVein()
		{
			return this.goType == GameObjectType.Vein;
		}

		/// <summary>
		/// Returns if the <see cref="Gameobject"/> is is of type <see cref="GameObjectType.Item"/>.
		/// </summary>
		public sealed override bool IsItem()
		{
			return this.goType == GameObjectType.Item;
		}

		/// <summary>
		/// Returns if the <see cref="Gameobject"/> is of type <see cref="GameObjectType.Chest"/>.
		/// </summary>
		public sealed override bool IsChest()
		{
			return this.goType == GameObjectType.Chest;
		}

		#endregion

		#region Property System Implementation

		/// <summary>
		/// Set interest flags
		/// </summary>
		/// <param name="flags"></param>
		public override void SetInterestFlags(InterestFlags flags)
		{
			// there is loot for the player
			if ((flags & InterestFlags.Loot) == InterestFlags.Loot)
			{
				if (!HasFlag(InterestFlags.Loot))
				{
					this.Avatar.CreateLootIndicator();
					this.SetFlags(InterestFlags.Loot);
				}
			}

			if ((flags & InterestFlags.Usable) == InterestFlags.Usable)
			{
				this.SetFlags(InterestFlags.Usable);
			}

			if ((flags & InterestFlags.Gatherable) == InterestFlags.Gatherable)
			{
				this.SetFlags(InterestFlags.Gatherable);
			}

			if ((flags & InterestFlags.Collectible) == InterestFlags.Collectible)
			{
				this.SetFlags(InterestFlags.Collectible);
			}

			// making this interactable if any flags are set
			if (!IsInteractable && this.Flags != InterestFlags.None)
			{
				this.SetInteractability(true);
			}
		}

		/// <summary>
		/// Unset interest flags
		/// </summary>
		/// <param name="flags"></param>
		public override void UnsetInterestFlags(InterestFlags flags)
		{
			// removing loot
			if ((flags & InterestFlags.Loot) == InterestFlags.Loot)
			{
				if (HasFlag(InterestFlags.Loot))
				{
					this.Avatar.ClearLootIndicator();
					this.UnsetFlags(InterestFlags.Loot | InterestFlags.Usable);
				}
			}

			if ((flags & InterestFlags.Usable) == InterestFlags.Usable)
			{
				this.UnsetFlags(InterestFlags.Usable);
			}

			if ((flags & InterestFlags.Gatherable) == InterestFlags.Gatherable)
			{
				this.UnsetFlags(InterestFlags.Gatherable);
			}

			if ((flags & InterestFlags.Collectible) == InterestFlags.Collectible)
			{
				this.UnsetFlags(InterestFlags.Collectible);
			}

			// making this non-interactable if all flags are unset
			if (IsInteractable && (this.Flags == InterestFlags.None))
			{
				this.SetInteractability(false);
			}
		}

		#endregion

		#region Loot System Implementation

		/// <summary>
		/// Tells whether this <see cref="Gameobject"/> has loot or not
		/// </summary>
		public override bool HaveLoot()
		{
			return this.loot != null;
		}

		/// <summary>
		/// Gets the <see cref="Loot"/> if there is loot
		/// </summary>
		public override Loot GetLoot()
		{
			return this.loot;
		}

		/// <summary>
		/// Sets the loot
		/// </summary>
		public override void SetLoot(Loot newLoot)
		{
			if(newLoot == null)
			{
				this.loot = null;
				if (HasFlag(InterestFlags.Loot))
				{
					this.Avatar.ClearLootIndicator();
					this.UnsetFlags(InterestFlags.Loot | InterestFlags.Usable);
				}
			}
			else
			{
				this.loot = newLoot;
				if (!HasFlag(InterestFlags.Loot))
				{
					this.Avatar.CreateLootIndicator();
					this.SetFlags(InterestFlags.Loot);
				}
			}
		}

		#endregion

		#region Movement System Implementation

		/// <summary>
		/// Sets the movement direction
		/// </summary>
		public override void SetDirection(MovementDirection value)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Sets the movement speed
		/// </summary>
		public override void SetSpeed(byte value)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Sets the movement state
		/// </summary>
		public override void SetMovementState(MovementState value)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
