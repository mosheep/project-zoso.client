using UnityEngine;
using System;

using Karen90MmoFramework.Game;

namespace Karen90MmoFramework.Client.Game.Objects
{
	public abstract class Character : MmoObject
	{
		#region Constants and Fields

		private readonly ObjectInfo charInfo = new ObjectInfo();

		private int currentSpeed;
		private Vector3 uVelocity;
		private Vector3 lerpFactor;

		private bool smoothRotate;

		private Species species;
		private Race race;

		private byte level;
		private int currHealth;
		private int currPower;
		private int maxHealth;
		private int maxPower;
		private UnitState state;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or Sets the properties info
		/// </summary>
		public ObjectInfo CharInfo
		{
			get
			{
				return this.charInfo;
			}
		}

		/// <summary>
		/// Gets the character's species
		/// </summary>
		public Species Species
		{
			get
			{
				return this.species;
			}
		}

		/// <summary>
		/// Gets the character's race
		/// </summary>
		public Race Race
		{
			get
			{
				return this.race;
			}
		}

		/// <summary>
		/// Gets the level
		/// </summary>
		public byte Level
		{
			get
			{
				return this.level;
			}
		}

		/// <summary>
		/// Gets the maximum health
		/// </summary>
		public virtual int MaximumHealth
		{
			get
			{
				return this.maxHealth;
			}
		}

		/// <summary>
		/// Gets the maximum power
		/// </summary>
		public virtual int MaximumPower
		{
			get
			{
				return this.maxPower;
			}
		}

		/// <summary>
		/// Gets the current health
		/// </summary>
		public int CurrentHealth
		{
			get
			{
				return this.currHealth;
			}
		}

		/// <summary>
		/// Gets the current power
		/// </summary>
		public int CurrentPower
		{
			get
			{
				return this.currPower;
			}
		}

		/// <summary>
		/// Gets the state
		/// </summary>
		public UnitState State
		{
			get
			{
				return this.state;
			}
		}
		
		/// <summary>
		/// Tells whether the character is dead or not
		/// </summary>
		public bool IsDead
		{
			get
			{
				return this.State != UnitState.Alive;
			}
		}

		/// <summary>
		/// Character Alignment
		/// </summary>
		public SocialAlignment Alignment { get; private set; }

		/// <summary>
		/// Speed of the character
		/// </summary>
		public float RelativeSpeed { get; protected set; }

		#endregion

		#region Constructors and Destructors

		protected Character(MmoWorld world, MmoGuid guid, IAvatar avatar)
			: base(world, guid, avatar)
		{
			uVelocity = Vector3.zero;
			lerpFactor = Vector3.zero;
			smoothRotate = false;
		}

		#endregion

		#region Game System Methods

		/// <summary>
		/// Called when the <see cref="MmoObject"/> is sapwned
		/// </summary>
		protected override void OnSpawn(Vector3 position, Vector3 rotation)
		{
			this.SetPosition(position, false);
			this.SetRotation(rotation, true);
			this.Avatar.Spawn(World.GetInterpolatedPosition(position));
			
			this.SetInteractability(true);
		}

		/// <summary>
		/// Called during update
		/// </summary>
		protected override void OnUpdate()
		{
			if (currentSpeed > 0)
			{
				// degrees to radians
				var rotRads = this.Rotation.y * Mathf.Deg2Rad;

				var sinT = (float) Math.Sin(rotRads);
				var cosT = (float) Math.Cos(rotRads);

				// calculating the direction vector
				var dx = this.uVelocity.x;
				var dz = this.uVelocity.z;
				var directionVector = new Vector3 {x = dz * sinT + dx * cosT, y = 0, z = dz * cosT - dx * sinT}.normalized;

				var newPosition = this.Position + (directionVector + lerpFactor) * this.currentSpeed * Time.deltaTime;
				newPosition = this.World.GetInterpolatedPosition(newPosition);
				this.SetPosition(newPosition, true);
			}

			if (smoothRotate)
			{
				var yRotationSelf = this.Rotation.y;
				var yRotationAvatar = this.Avatar.Rotation.y;
				if (Math.Abs(yRotationSelf - yRotationAvatar) > 0.05f)
				{
					var newRotation = this.Avatar.Rotation;
					newRotation.y = Mathf.LerpAngle(yRotationAvatar, yRotationSelf, Time.deltaTime * GameSettings.NPC_SMOOTH_ORIENTATION_FACTOR);
					this.Avatar.SetRotation(newRotation);
				}

				var xRotationSelf = this.Rotation.x;
				var xRotationAvatar = this.Avatar.Rotation.x;
				if (Math.Abs(xRotationSelf - xRotationAvatar) > 0.05f)
				{
					var newRotation = this.Avatar.Rotation;
					newRotation.x = Mathf.LerpAngle(xRotationAvatar, xRotationSelf, Time.deltaTime * GameSettings.NPC_SMOOTH_ORIENTATION_FACTOR);
					this.Avatar.SetRotation(newRotation);
				}
			}
		}

		/// <summary>
		/// Called when the <see cref="Character"/> is destroyed
		/// </summary>
		protected override void OnDestroy()
		{
			this.SetInteractability(false);

			if (World.Player.CurrentInteractor == this)
				World.Player.CloseInteraction();

			if (World.Player.CurrentTarget == this)
				World.Player.CloseTarget();
		}

		#endregion

		#region Property System Implementation

		/// <summary>
		/// Called when a property has been set in the <see cref="System.Collections.Hashtable"/>
		/// </summary>
		protected override void OnPropertySet(PropertyCode propertiesCode, object data)
		{
			switch (propertiesCode)
			{
				case PropertyCode.Name:
					this.SetName(data as string);
					this.CharInfo.ExtendedName = string.Format("{0} [{1}]", this.Name, this.Level);
					break;

				case PropertyCode.UnitState:
					var oldState = this.State;
					var newState = (UnitState) data;

					switch (newState)
					{
						case UnitState.Dead:
							{
								if (oldState != UnitState.Dead)
									this.Kill();
							}
							break;

						default:
							{
								this.SetUnitState(newState);
							}
							break;
					}
					break;

				case PropertyCode.Species:
					this.species = (Species) data;
					break;

				case PropertyCode.Race:
					this.race = (Race) data;
					break;

				case PropertyCode.Level:
					this.SetLevel((byte) data);
					break;

				case PropertyCode.Alignment:
					this.Alignment = (SocialAlignment) data;
					break;

				case PropertyCode.MaxHp:
					this.SetMaxHealth((short) data);
					break;

				case PropertyCode.CurrHp:
					this.SetHealth((int) data);
					break;

				case PropertyCode.MaxPow:
					this.SetMaxPower((short) data);
					break;

				case PropertyCode.CurrPow:
					this.SetPower((int) data);
					break;

				default:
					base.OnPropertySet(propertiesCode, data);
					break;
			}
		}

		/// <summary>
		/// Sets the current level
		/// </summary>
		protected void SetLevel(byte value)
		{
			var maxLevel = Guid.Type == (byte)ObjectType.Player ? GlobalGameSettings.MAX_PLAYER_LEVEL : GlobalGameSettings.MAX_NPC_LEVEL;
			if (value == this.level || value > maxLevel)
				return;

			this.level = (byte)Mathf.Clamp(value, 1, maxLevel);
			this.Revision++;

			this.CharInfo.ExtendedName = string.Format("{0} [{1}]", this.Name, this.Level);
		}

		/// <summary>
		/// Sets health
		/// </summary>
		protected void SetHealth(int value)
		{
			if (currHealth == value || value < 0)
				return;

			this.currHealth = value;
			this.Revision++;

			this.CharInfo.HealthInfo = string.Format("{0} / {1}", this.CurrentHealth, this.MaximumHealth);
		}

		/// <summary>
		/// Sets max health
		/// </summary>
		protected void SetMaxHealth(int value)
		{
			if (maxHealth == value || value < 1)
				return;

			this.maxHealth = value;
			this.Revision++;
			
			this.CharInfo.HealthInfo = string.Format("{0} / {1}", this.CurrentHealth, this.MaximumHealth);
		}

		/// <summary>
		/// Sets power
		/// </summary>
		protected void SetPower(int value)
		{
			if (currPower == value || value < 0)
				return;

			// power will not be set in the property hashtable
			// because it will not be visible to any players
			this.currPower = value;
			if (Guid == World.Player.Guid)
				this.CharInfo.PowerInfo = string.Format("{0} / {1}", this.CurrentPower, this.MaximumPower);
		}

		/// <summary>
		/// Sets max power
		/// </summary>
		protected void SetMaxPower(int value)
		{
			if (maxPower == value || value < 1)
				return;

			// max power will not be set in the property hashtable
			// because it will not be visible to any players
			this.maxPower = value;
			if (Guid == World.Player.Guid)
				this.CharInfo.PowerInfo = string.Format("{0} / {1}", this.CurrentPower, this.MaximumPower);
		}

		/// <summary>
		/// Sets the character state
		/// </summary>
		protected void SetUnitState(UnitState newState)
		{
			if (state == newState)
				return;

			this.state = newState;
			this.Revision++;
		}

		#endregion

		#region Combat System Implementation

		/// <summary>
		/// Tells whether the character has a certain amount of vital or not
		/// </summary>
		public bool HaveVital(Vitals vital, int value)
		{
			switch (vital)
			{
				case Vitals.Health:
					return currHealth > value; // to avoid death

				case Vitals.Power:
					return currPower >= value;
			}

			return false;
		}

		/// <summary>
		/// Kills the character
		/// </summary>
		protected void Kill()
		{
			if (state == UnitState.Dead)
				return;

			this.SetUnitState(UnitState.Dead);
			this.SetHealth(0);
			this.SetPower(0);

			this.OnDeath();
		}

		/// <summary>
		/// Called when this <see cref="Character"/> is killed.
		/// </summary>
		protected virtual void OnDeath()
		{
			this.currentSpeed = 0;
		}

		#endregion

		#region Movement System Implementation

		/// <summary>
		/// Smoothly moves to a position
		/// </summary>
		public override void Move(Vector3 newPosition)
		{
			var distance = this.Position.GetDistance(newPosition);
			if (distance >= GameSettings.MIN_INSTANT_SNAP_DISTANCE)
			{
				newPosition = this.World.GetInterpolatedPosition(newPosition);
				this.SetPosition(newPosition, true);

				this.lerpFactor = Vector3.zero;

				return;
			}

			if (distance < GameSettings.MIN_IGNORE_LERP_DISTANCE)
			{
				this.lerpFactor = Vector3.zero;
				return;
			}

			if (currentSpeed > 0)
			{
				this.lerpFactor = (newPosition - this.Position).normalized / this.currentSpeed;
			}
		}

		/// <summary>
		/// Smoothly rotates to the new rotation
		/// </summary>
		public override sealed void Rotate(Vector3 newRotation)
		{
			this.smoothRotate = true;
			// if the new rotation is off by a lot set the our and the avatar's rotation instantly
			if (Math.Abs(Rotation.y - newRotation.y) > GameSettings.MIN_INSTANT_ROTATION_ANGLE
				|| Math.Abs(Rotation.x - newRotation.x) > GameSettings.MIN_INSTANT_ROTATION_ANGLE)
			{
				this.SetRotation(newRotation, true);
				this.smoothRotate = false;
			}

			// if it is not set only our rotation so it will be smoothly rotated inside the update function
			if (smoothRotate)
				this.SetRotation(newRotation, false);
		}

		/// <summary>
		/// Sets the movement direction
		/// </summary>
		public override void SetDirection(MovementDirection value)
		{
			var dx = this.GetUnitSpeedX(value);
			var dz = this.GetUnitSpeedZ(value);

			this.uVelocity = new Vector3 { x = dx, y = 0, z = dz };

			this.Avatar.SetDirection(value);
		}

		/// <summary>
		/// Sets the movement speed
		/// </summary>
		public override void SetSpeed(byte value)
		{
			this.currentSpeed = value;
		}

		/// <summary>
		/// Sets the movement state
		/// </summary>
		public override void SetMovementState(MovementState value)
		{
			this.Avatar.SetState(value);
		}

		/// <summary>
		/// Gets the unit x speed based on the keys pressed
		/// </summary>
		protected int GetUnitSpeedX(MovementDirection direction)
		{
			var dx = 0;
			if ((direction & MovementDirection.Right) == MovementDirection.Right)
				dx++;

			if ((direction & MovementDirection.Left) == MovementDirection.Left)
				dx--;

			return dx;
		}

		/// <summary>
		/// Gets the unit z speed based on the keys pressed
		/// </summary>
		protected int GetUnitSpeedZ(MovementDirection direction)
		{
			var dz = 0;
			if ((direction & MovementDirection.Forward) == MovementDirection.Forward)
				dz++;

			if ((direction & MovementDirection.Backward) == MovementDirection.Backward)
				dz--;

			return dz;
		}

		#endregion
	}
}