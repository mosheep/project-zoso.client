﻿namespace Karen90MmoFramework.Client.Game.Systems
{
	public enum InventoryOperationResult : byte
	{
		/// <summary>
		/// Success
		/// </summary>
		Success,

		/// <summary>
		/// Fail
		/// </summary>
		Fail,

		/// <summary>
		/// Added partially
		/// </summary>
		PartialAdditionDueToSpace,

		/// <summary>
		/// Removed partialy
		/// </summary>
		PartialRemoval,

		/// <summary>
		/// Container full
		/// </summary>
		NoAdditionDueToSpace,

		/// <summary>
		/// No item found
		/// </summary>
		NoRemovalDueToItemNotFound,

		/// <summary>
		/// Non collectible item
		/// </summary>
		NonCollectibleItem,

		/// <summary>
		/// Operation not allowed
		/// </summary>
		NotAllowed,
	};
}
