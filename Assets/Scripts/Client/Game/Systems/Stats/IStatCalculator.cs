﻿using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Game.Objects;

namespace Karen90MmoFramework.Client.Game.Systems
{
	public interface IStatCalculator<out T>
	{
		/// <summary>
		/// Calculates the resultant value of a <see cref="Stats"/>.
		/// </summary>
		T CalculateValue(Player player, Stats stat);
	}
}
