﻿using System.Collections.Generic;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Hud;
using Karen90MmoFramework.Client.Data;
using Karen90MmoFramework.Client.Game.Objects;

namespace Karen90MmoFramework.Client.Game.Systems
{
	public class SpellManager : ISpellController
	{
		#region Constants and Fields

		/// <summary>
		/// Global Cooldown
		/// </summary>
		public const float GCD = 1.0f;

		/// <summary>
		/// spell collection
		/// </summary>
		private readonly Dictionary<short, Spell> spells;

		/// <summary>
		/// spell updates
		/// </summary>
		private readonly Dictionary<short, Spell> updatingSpells;

		/// <summary>
		/// dirty spells
		/// </summary>
		private readonly List<short> dirtySpells;

		/// <summary>
		/// the owner
		/// </summary>
		private readonly Player player;

		private readonly Spell useSpell;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the spells
		/// </summary>
		public IEnumerable<Spell> Spells
		{
			get
			{
				return this.spells.Values;
			}
		}

		/// <summary>
		/// Tells whether GCD has been activated or not
		/// </summary>
		public bool InGCD { get; private set; }

		/// <summary>
		/// Gets the GCD timer
		/// </summary>
		public float GCDTimer { get; private set; }

		#endregion

		#region Constructors and Destructors

		/// <summary>
		///	Creates an instance of <see cref="SpellManager"/>.
		/// </summary>
		public SpellManager(Player player)
		{
			this.player = player;

			this.spells = new Dictionary<short, Spell>();
			this.updatingSpells = new Dictionary<short, Spell>();
			this.dirtySpells = new List<short>();

			this.InGCD = false;
			this.GCDTimer = 0;

			SpellData spellData;
			if (MmoItemDataStorage.Instance.TryGetSpell(GameSettings.USE_SPELL_ID, out spellData))
			{
				this.useSpell = new Spell(spellData);
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Adds a spell and initializes it with the owner as the caster.
		/// </summary>
		public bool AddSpell(short spellId)
		{
			if (!spells.ContainsKey(spellId))
			{
				SpellData spellData;
				if (MmoItemDataStorage.Instance.TryGetSpell(spellId, out spellData))
				{
					var spell = new Spell(spellData);
					this.spells.Add(spell.Id, spell);
					spell.Initialize(player, this);

					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Removes a spell with an Id.
		/// </summary>
		public bool RemoveSpell(short id)
		{
			return this.spells.Remove(id);
		}

		/// <summary>
		/// Tries to retrieve a spell
		/// </summary>
		public bool TryGetSpell(short id, out Spell spell)
		{
			return this.spells.TryGetValue(id, out spell);
		}

		/// <summary>
		/// Updates the <see cref="SpellManager"/>.
		/// </summary>
		public void Update(float deltaTime)
		{
			if (InGCD)
			{
				GCDTimer += deltaTime;
				if (GCDTimer >= GCD)
				{
					this.InGCD = false;
					this.GCDTimer = 0;
				}
			}

			if (updatingSpells.Count > 0)
			{
				foreach (var spell in updatingSpells.Values)
				{
					spell.Update(deltaTime);
				}
			}

			if (dirtySpells.Count > 0)
			{
				foreach (var spellId in dirtySpells)
				{
					this.updatingSpells.Remove(spellId);
				}

				this.dirtySpells.Clear();
			}
		}

		/// <summary>
		/// Casts a spell
		/// </summary>
		public CastResults CastSpell(short id, MmoObject target)
		{
			Spell spell;
			if (spells.TryGetValue(id, out spell))
			{
				if (spell.AffectedByGCD && this.InGCD)
					return CastResults.InGcd;

				var result = spell.CheckSpell(target);
				if (result == CastResults.Ok)
				{
					MmoGuid? guid = null;
					if (target != null && target != player)
						guid = target.Guid;

					Operations.CastSpell(id, guid);
				}
				else
				{
					player.World.Game.Chat.PostMessage(MessageType.Game, result.ToResultCode().GetErrorMessage());
				}

				return result;
			}

			return CastResults.SpellNotReady;
		}

		/// <summary>
		/// Begins casting a spell
		/// </summary>
		public void BeginCast(short id)
		{
			Spell spell;
			if (spells.TryGetValue(id, out spell))
			{
				HUD.Instance.BeginCasting(spell);
			}
		}

		/// <summary>
		/// Ends the current spell cast
		/// </summary>
		public void EndCast()
		{
			HUD.Instance.EndCasting();
		}

		/// <summary>
		/// Begins use spell
		/// </summary>
		public void BeginUse()
		{
			if (useSpell != null)
			{
				HUD.Instance.BeginCasting(useSpell);
			}
		}

		/// <summary>
		/// Starts the global cooldown
		/// </summary>
		public void BeginGCD()
		{
			this.InGCD = true;
			this.GCDTimer = 0;
		}

		/// <summary>
		/// Ends the global cooldown
		/// </summary>
		public void EndGCD()
		{
			this.InGCD = false;
		}

		/// <summary>
		/// Adds a spell update event
		/// </summary>
		/// <param name="spell"></param>
		void ISpellController.AddSpellUpdateEvent(Spell spell)
		{
			this.updatingSpells.Add(spell.Id, spell);
		}

		/// <summary>
		/// Removes a spell update event
		/// </summary>
		/// <param name="spell"></param>
		void ISpellController.RemoveSpellUpdateEvent(Spell spell)
		{
			// not directly removing the spell is to avoid iterator issues
			this.dirtySpells.Add(spell.Id);
		}

		#endregion
	}
}
