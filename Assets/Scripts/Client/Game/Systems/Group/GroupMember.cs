﻿using System.Collections;
using UnityEngine;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Game.Objects;

namespace Karen90MmoFramework.Client.Game.Systems
{
	public class GroupMember
	{
		#region Constants and Fields

		private readonly MmoGuid guid;
		private readonly ObjectInfo charInfo;

		private string name;
		private byte level;
		private GroupMemberStatus status;
		private Texture2D portrait;

		private int maxHp;
		private int maxPow;
		private int currHp;
		private int currPow;

		private Vector3 pos;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets or Sets the Link if any
		/// </summary>
		public OtherPlayer Link { get; set; }

		/// <summary>
		/// Gets or Sets the status
		/// </summary>
		public GroupMemberStatus Status
		{
			get { return status; }
			set
			{
				status = value;
				switch (value)
				{
					case GroupMemberStatus.Dead:
						{
							this.charInfo.ExtendedName = this.Name + "(Dead)";
						}
						break;

					case GroupMemberStatus.Offline:
						{
							this.charInfo.ExtendedName = this.Name + "(Offline)";
						}
						break;

					case GroupMemberStatus.Online:
						{
							this.charInfo.ExtendedName = this.Name;
						}
						break;
				}
			}
		}

		/// <summary>
		/// Gets the guid
		/// </summary>
		public MmoGuid Guid
		{
			get { return this.guid; }
		}

		/// <summary>
		/// Gets the character info
		/// </summary>
		public ObjectInfo CharInfo
		{
			get { if (Link != null) return Link.CharInfo; return this.charInfo; }
		}

		/// <summary>
		/// Gets or Sets the name
		/// </summary>
		public string Name
		{
			get { if (Link != null) return Link.Name; return this.name; }
			set
			{
				this.name = value.CapFirstLetter();
				this.charInfo.ExtendedName = this.name;
			}
		}

		/// <summary>
		/// Gets or Sets the level
		/// </summary>
		public byte Level
		{
			get { if (Link != null) return Link.Level; return this.level; }
			set { this.level = value; }
		}

		/// <summary>
		/// Gets or Sets the position
		/// </summary>
		private Vector3 Position
		{
			get { if (Link != null) return Link.Position; return this.pos; }
			set { this.pos = value; }
		}

		/// <summary>
		/// Gets or Sets the max hp
		/// </summary>
		public int MaxHp
		{
			get
			{
				if (Link != null)
					return Link.MaximumHealth;
				return this.maxHp;
			}
			set
			{
				this.maxHp = value;
				this.charInfo.HealthInfo = this.currHp + " / " + this.maxHp;
			}
		}

		/// <summary>
		/// Gets or Sets the current hp
		/// </summary>
		public int CurrHp
		{
			get
			{
				if (Link != null) 
					return Link.CurrentHealth; 
				return this.currHp;
			}
			set
			{
				this.currHp = value;
				this.charInfo.HealthInfo = this.currHp + " / " + this.maxHp;
			}
		}

		#endregion

		#region Constructor and Destructor

		public GroupMember(MmoGuid guid)
		{
			this.guid = guid;
			this.charInfo = new ObjectInfo();
			this.status = GroupMemberStatus.Online; // temp
		}

		#endregion

		#region Public Methods

		public void CopyLink()
		{
			if (Link == null)
				return;

			this.Name = this.Link.Name;
			this.Level = this.Link.Level;
			this.Position = this.Link.Position;
			this.MaxHp = this.Link.MaximumHealth;
			this.CurrHp = this.Link.CurrentHealth;
		}

		public void SetProperties(IDictionary properties)
		{
			foreach (DictionaryEntry pair in properties)
				this.SetProperty((GroupMemberProperty)pair.Key, pair.Value);
		}

		public void SetProperty(GroupMemberProperty property, object value)
		{
			switch (property)
			{
				case GroupMemberProperty.Name:
					{
						this.Name = value as string;
					}
					break;

				case GroupMemberProperty.Level:
					{
						this.Level = (byte) value;
						this.charInfo.ExtendedName = string.Format("{0} [{1}]", this.Name, this.Level);
					}
					break;

				case GroupMemberProperty.MaxHp:
					{
						this.MaxHp = (int)value;
					}
					break;

				case GroupMemberProperty.CurrHp:
					{
						this.CurrHp = (int)value;
					}
					break;

				case GroupMemberProperty.Position:
					{
						var pArr = (int[]) value;
						this.Position = new Vector3() {x = pArr[0], y = pArr[1]};
					}
					break;

				case GroupMemberProperty.Status:
					{
						this.Status = (GroupMemberStatus)value;
					}
					break;
			}
		}

		#endregion
	}
}

