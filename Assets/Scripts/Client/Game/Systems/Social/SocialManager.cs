﻿using System;
using System.Collections.Generic;
using Karen90MmoFramework.Game;

namespace Karen90MmoFramework.Client.Game.Systems
{
	public class SocialManager
	{
		#region Constants and Fields

		private readonly Dictionary<string, Friend> friendsCollection;
		private readonly List<Friend> onlineFriends;
		private readonly List<Friend> offlineFriends;
		private readonly List<string> receivedFriendRequests;

		#endregion

		#region Constructors and Destructors

		public SocialManager()
		{
			this.friendsCollection = new Dictionary<string, Friend>(StringComparer.CurrentCultureIgnoreCase);
			this.onlineFriends = new List<Friend>();
			this.offlineFriends = new List<Friend>();
			this.receivedFriendRequests = new List<string>();
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets all the friends
		/// </summary>
		public IEnumerable<Friend> Friends
		{
			get { return friendsCollection.Values; }
		}

		/// <summary>
		/// Gets all the online friends
		/// </summary>
		public IEnumerable<Friend> OnlineFriends
		{
			get { return onlineFriends; }
		}
		
		/// <summary>
		/// Gets all the offline friends
		/// </summary>
		public IEnumerable<Friend> OfflineFriends
		{
			get { return offlineFriends; }
		}

		/// <summary>
		/// Gets all received friend requests
		/// </summary>
		public List<string> ReceivedFriendRequests
		{
			get
			{
				return this.receivedFriendRequests;
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Receives a friend request from a player
		/// </summary>
		/// <param name="nameOfRequester"></param>
		public void ReceiveFriendRequest(string nameOfRequester)
		{
			this.receivedFriendRequests.Add(nameOfRequester);
		}

		/// <summary>
		/// Cancels a friend request
		/// </summary>
		/// <param name="nameOfRequester"></param>
		public void CancelFriendRequest(string nameOfRequester)
		{
			this.receivedFriendRequests.Remove(nameOfRequester);
		}

		/// <summary>
		/// Adds a friend
		/// </summary>
		/// <param name="profileInfo"></param>
		public void AddFriend(ProfileStructure profileInfo)
		{
			var friend = new Friend(profileInfo.Name, (SocialStatus) profileInfo.OnlineStatus);
			this.friendsCollection.Add(friend.Name, friend);

			if(friend.OnlineStatus == SocialStatus.Offline)
			{
				this.offlineFriends.Add(friend);
			}
			else
			{
				this.onlineFriends.Add(friend);
			}
		}

		/// <summary>
		/// Adds a friend
		/// </summary>
		/// <param name="nameOfFriend"></param>
		public void AddFriend(string nameOfFriend)
		{
			var friend = new Friend(nameOfFriend, SocialStatus.Offline);
			this.friendsCollection.Add(friend.Name, friend);
			this.offlineFriends.Add(friend);
		}

		/// <summary>
		/// Updates a friend
		/// </summary>
		/// <param name="nameOfFriend"> </param>
		/// <param name="profileInfo"></param>
		public void UpdateFriend(string nameOfFriend, ProfileStructure profileInfo)
		{
			Friend friend;
			if (!friendsCollection.TryGetValue(nameOfFriend, out friend))
				return;

			var newStatus = (SocialStatus) profileInfo.OnlineStatus;
			if(friend.OnlineStatus != newStatus)
				this.DoUpdateStatus(friend, newStatus);

			this.DoUpdateProfile(friend, profileInfo);
		}

		/// <summary>
		/// Removes a friend
		/// </summary>
		/// <param name="nameOfFriend"></param>
		public void RemoveFriend(string nameOfFriend)
		{
			Friend friend;
			if(!friendsCollection.TryGetValue(nameOfFriend, out friend))
				return;

			if (friend.OnlineStatus == SocialStatus.Offline)
			{
				this.offlineFriends.Remove(friend);
			}
			else
			{
				this.onlineFriends.Remove(friend);
			}
		}

		/// <summary>
		/// Updates the status of a friend
		/// </summary>
		/// <param name="nameOfFriend"></param>
		/// <param name="newStatus"></param>
		public void UpdateStatus(string nameOfFriend, SocialStatus newStatus)
		{
			Friend friend;
			if (!friendsCollection.TryGetValue(nameOfFriend, out friend))
				return;

			this.DoUpdateStatus(friend, newStatus);
		}

		void DoUpdateStatus(Friend friend, SocialStatus newStatus)
		{
			var oldStatus = friend.OnlineStatus;
			friend.OnlineStatus = newStatus;

			if (oldStatus == SocialStatus.Offline)
			{
				if (newStatus != SocialStatus.Offline)
				{
					this.offlineFriends.Remove(friend);
					this.onlineFriends.Add(friend);
				}
			}
			else
			{
				if (newStatus == SocialStatus.Offline)
				{
					this.onlineFriends.Remove(friend);
					this.offlineFriends.Add(friend);
				}
			}
		}

		void DoUpdateProfile(Friend friend, ProfileStructure profileInfo)
		{
			friend.Level = profileInfo.Level;
			friend.World = GlobalGameSettings.WorldIdToWorldName(profileInfo.WorldId);
		}

		#endregion
	}
}
