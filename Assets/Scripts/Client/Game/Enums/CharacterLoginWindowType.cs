﻿namespace Karen90MmoFramework.Client.Game
{
	public enum CharacterLoginWindowType
	{
		Splash = 0,
		CharacterSelection = 1,
		CharacterCreation = 2,
	};
}