﻿namespace Karen90MmoFramework.Client.Game
{
	public enum LootItemStatus : byte
	{
		Unlooted		= 0,
		Looting			= 1,
		Looted			= 2,
	};
}