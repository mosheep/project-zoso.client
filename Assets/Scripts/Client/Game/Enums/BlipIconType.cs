﻿namespace Karen90MmoFramework.Client.Game
{
	public enum BlipIconType : byte
	{
		None				= 0,
		QuestActive			= 1,
		QuestInProgress		= 2,
		QuestTurnIn			= 3,
		Merchant			= 4,
		GroupMember			= 5,
	};
}