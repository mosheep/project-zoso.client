﻿namespace Karen90MmoFramework.Client.Game
{
	public enum CharacterWindowType : byte
	{
		WINDOW_STATS			= 0,
		WINDOW_ABILITIES		= 1,
		WINDOW_REPUTATION		= 2,
	};
}