﻿using System;
using ExitGames.Client.Photon;

namespace Karen90MmoFramework.Client
{
	public interface IMessageDispatcher
	{
		/// <summary>
		/// Adds an operation handler to be called when an <see cref="OperationResponse"/> is received. If an existing operation handler
		/// is found it will be invoked and a value of <value>NULL</value> will be passed and removed before adding the new handler.
		/// There can be only one operation handler for a particular <see cref="OperationResponse"/>.
		/// </summary>
		/// <param name="operationCode"></param>
		/// <param name="responseHandler"></param>
		/// <returns></returns>
		IDisposable RegisterOperationResponseHandler(byte operationCode, Action<OperationResponse> responseHandler);

		/// <summary>
		/// Adds an event handler to be called when an <see cref="EventData"/> is received. If an existing event handler
		/// is found it will be invoked and a value of <value>NULL</value> will be passed and removed before adding the new handler.
		/// There can be only one event handler for a particular <see cref="EventData"/>.
		/// </summary>
		/// <param name="eventCode"> </param>
		/// <param name="eventHandler"></param>
		/// <returns></returns>
		IDisposable RegisterEventHandler(byte eventCode, Action<EventData> eventHandler);
	}
}
