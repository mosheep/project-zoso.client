﻿using System.Collections.Generic;

using ExitGames.Client.Photon;
using Karen90MmoFramework.Rpc;

namespace Karen90MmoFramework.Client
{
	public class Disconnected : IConnectionState
	{
		#region Constants and Fields

		/// <summary>
		/// Gets the singleton instance
		/// </summary>
		public static Disconnected Instance = new Disconnected();

		/// <summary>
		/// Gets the state
		/// </summary>
		public IConnectionState State
		{
			get
			{
				return this;
			}
		}

		#endregion

		#region IConnectionState Implementation

		/// <summary>
		/// Gets the connection status
		/// </summary>
		ConnectionStatus IConnectionState.Status
		{
			get
			{
				return ConnectionStatus.Disconnected;
			}
		}

		/// <summary>
		/// Handles a(n) <see cref="OperationResponse"/>.
		/// </summary>
		bool IConnectionState.HandleOperationResponse(OperationResponse operationResponse)
		{
			return false;
		}

		/// <summary>
		/// Handles a(n) <see cref="EventData"/>.
		/// </summary>
		bool IConnectionState.HandleEvent(EventData eventData)
		{
			return false;
		}

		/// <summary>
		/// Sends an operation
		/// </summary>
		void IConnectionState.SendOperation(PhotonPeer peer, ClientOperationCode operationCode, Dictionary<byte, object> parameters, bool encrypt)
		{
			Logger.DebugFormat("Cannot send operation request while State={0}", State.Status);
		}

		#endregion
	}
}