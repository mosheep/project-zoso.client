﻿using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;

using UnityEngine;
using ExitGames.Client.Photon;

using Karen90MmoFramework.Rpc;
using Karen90MmoFramework.Hud;
using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Data;
using Karen90MmoFramework.Client.Game;
using Karen90MmoFramework.Client.Game.Systems;
using Karen90MmoFramework.Client.Game.Objects;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace Karen90MmoFramework.Client
{
	public sealed class WorldEntered : IConnectionState
	{
		#region Constants and Fields

		/// <summary>
		/// Gets the singleton instance
		/// </summary>
		public static readonly WorldEntered Instance = new WorldEntered();

		/// <summary>
		/// Gets the state
		/// </summary>
		public IConnectionState State
		{
			get
			{
				return this;
			}
		}

		/// <summary>
		/// Gets or sets the world
		/// </summary>
		public MmoWorld World { get; set; }

		#endregion

		#region IGameState Implementation

		/// <summary>
		/// Gets the connection status
		/// </summary>
		ConnectionStatus IConnectionState.Status
		{
			get
			{
				return ConnectionStatus.WorldEntered;
			}
		}

		/// <summary>
		/// Handles a(n) <see cref="EventData"/>.
		/// </summary>
		bool IConnectionState.HandleEvent(EventData eventData)
		{
			switch ((ClientEventCode)eventData.Code)
			{
				case ClientEventCode.ObjectDestroyed:
					HandleEventObjectDestroyed(eventData);
					return true;

				case ClientEventCode.ObjectSubscribed:
					HandleEventObjectSubscribed(eventData);
					return true;

				case ClientEventCode.ObjectUnsubscribed:
					HandlEventObjectUnsubscribed(eventData);
					return true;

				case ClientEventCode.ObjectMovement:
					HandleEventObjectMovement(eventData);
					return true;

				case ClientEventCode.ObjectTransform:
					HandleEventObjectTransform(eventData);
					return true;

				case ClientEventCode.ObjectProperty:
					HandleEventObjectProperty(eventData);
					return true;

				case ClientEventCode.ObjectPropertyMultiple:
					HandleEventObjectPropertyMultiple(eventData);
					return true;

				case ClientEventCode.ObjectFlagsSet:
					HandleEventObjectFlagsSet(eventData);
					return true;

				case ClientEventCode.ObjectFlagsUnset:
					HandleEventObjectFlagsUnset(eventData);
					return true;

				case ClientEventCode.ObjectLevelUp:
					HandleEventObjectLevelUp(eventData);
					return true;

				case ClientEventCode.YouLevelUp:
					HandleEventYouLevelUp(eventData);
					return true;

				case ClientEventCode.InteractionShop:
					HandleEventInteractionShop(eventData);
					return true;

				case ClientEventCode.InteractionShopList:
					HandleEventInteractionShopList(eventData);
					return true;

				case ClientEventCode.InteractionLoot:
					HandleEventInteractionLoot(eventData);
					return true;

				case ClientEventCode.InteractionLootList:
					HandleEventInteractionLootList(eventData);
					return true;

				case ClientEventCode.InteractionDialogue:
					HandleEventInteractionDialogue(eventData);
					return true;

				case ClientEventCode.InteractionDialogueList:
					HandleEventInteractionDialogueList(eventData);
					return true;

				case ClientEventCode.InventoryInit:
					HandleEventInventoryInit(eventData);
					return true;

				case ClientEventCode.InventoryItemAdded:
					HandleEventInventoryItemAdded(eventData);
					return true;

				case ClientEventCode.InventoryItemAddedMultiple:
					HandleEventInventoryItemAddedMultiple(eventData);
					return true;

				case ClientEventCode.InventoryItemRemoved:
					HandleEventInventoryItemRemoved(eventData);
					return true;

				case ClientEventCode.InventoryItemMoved:
					HandleEventInventoryItemMoved(eventData);
					return true;

				case ClientEventCode.ActionbarInit:
					HandleEventActionBarInit(eventData);
					return true;

				case ClientEventCode.SpellManagerInit:
					HandleEventSpellManagerInit(eventData);
					return true;

				case ClientEventCode.SpellAdded:
					HandleEventSpellAdded(eventData);
					return true;

				case ClientEventCode.SpellRemoved:
					HandleEventSpellRemoved(eventData);
					return true;

				case ClientEventCode.SpellCastBegin:
					HandleEventSpellCastBegin(eventData);
					return true;

				case ClientEventCode.SpellCastEnd:
					HandleEventSpellCastEnd(eventData);
					return true;

				case ClientEventCode.SpellCooldownBegin:
					HandleEventSpellCooldownBegin(eventData);
					return true;

				case ClientEventCode.SpellCooldownEnd:
					HandleEventSpellCooldownEnd(eventData);
					return true;

				case ClientEventCode.LootInit:
					HandleEventLootInit(eventData);
					return true;

				case ClientEventCode.LootClear:
					HandleEventLootClear(eventData);
					return true;

				case ClientEventCode.LootItemRemoved:
					HandleEventLootItemRemoved(eventData);
					return true;

				case ClientEventCode.LootGoldRemoved:
					HandleEventLootGoldRemoved(eventData);
					return true;

				case ClientEventCode.QuestManagerInit:
					HandleEventQuestManagerInit(eventData);
					return true;

				case ClientEventCode.QuestGiverStatus:
					HandleEventQuestGiverStatus(eventData);
					return true;

				case ClientEventCode.QuestStarted:
					HandleEventQuestStarted(eventData);
					return true;

				case ClientEventCode.QuestFinished:
					HandleEventQuestFinished(eventData);
					return true;

				case ClientEventCode.QuestProgress:
					HandleEventQuestProgress(eventData);
					return true;

				case ClientEventCode.QuestRegress:
					HandleEventQuestRegress(eventData);
					return true;

				case ClientEventCode.QuestAdded:
					HandleEventQuestAdded(eventData);
					return true;

				case ClientEventCode.ChatChannelJoined:
					HandleEventChatChannelJoined(eventData);
					return true;

				case ClientEventCode.ChatChannelLeft:
					HandleEventChatChannelLeft(eventData);
					return true;

				case ClientEventCode.ChatMessageReceived:
					HandleEventChatMessageReceived(eventData);
					return true;

				case ClientEventCode.SocialFriendAddedName:
					HandleEventSocialFriendAddedName(eventData);
					return true;

				case ClientEventCode.SocialFriendAddedNameMultiple:
					HandleEventSocialFriendAddedNameMultiple(eventData);
					return true;

				case ClientEventCode.SocialFriendAddedData:
					HandleEventSocialFriendAddedData(eventData);
					return true;

				case ClientEventCode.SocialFriendAddedDataMultiple:
					HandleEventSocialFriendAddedDataMultiple(eventData);
					return true;

				case ClientEventCode.SocialIgnoreAddedName:
					HandleEventSocialIgnoreAddedName(eventData);
					return true;

				case ClientEventCode.SocialIgnoreAddedNameMultiple:
					HandleEventSocialIgnoreAddedNameMultiple(eventData);
					return true;

				case ClientEventCode.SocialProfileUpdate:
					HandleEventSocialProfileUpdate(eventData);
					return true;

				case ClientEventCode.SocialProfileUpdateMultiple:
					HandleEventSocialProfileUpdateMultiple(eventData);
					return true;

				case ClientEventCode.SocialProfileRemoved:
					HandleEventSocialProfileRemoved(eventData);
					return true;

				case ClientEventCode.SocialProfileStatus:
					HandleEventSocialProfileStatus(eventData);
					return true;

				case ClientEventCode.SocialFriendRequestReceived:
					HandleEventSocialFriendRequestReceived(eventData);
					return true;

				case ClientEventCode.SocialFriendRequestCancelled:
					HandleEventSocialFriendRequestCancelled(eventData);
					return true;

				case ClientEventCode.GroupInit:
					HandleEventGroupInit(eventData);
					return true;

				case ClientEventCode.GroupMemberAdded:
					HandleEventGroupMemberAdded(eventData);
					return true;

				case ClientEventCode.GroupMemberAddedGuid:
					HandleEventGroupMemberAddedGuid(eventData);
					return true;

				case ClientEventCode.GroupMemberAddedProperties:
					HandleEventGroupMemberAddedProperties(eventData);
					return true;

				case ClientEventCode.GroupMemberAddedInactive:
					HandleEventGroupMemberAddedInactive(eventData);
					return true;

				case ClientEventCode.GroupMemberRemoved:
					HandleEventGroupMemberRemoved(eventData);
					return true;

				case ClientEventCode.GroupMemberUpdate:
					HandleEventGroupMemberUpdate(eventData);
					return true;

				case ClientEventCode.GroupMemberDisconnected:
					HandleEventGroupMemberDisconnected(eventData);
					return true;

				case ClientEventCode.GroupUninvited:
					HandleEventGroupUninvited(eventData);
					return true;

				case ClientEventCode.GroupDisbanded:
					HandleEventGroupDisbanded(eventData);
					return true;

				case ClientEventCode.GroupInviteReceived:
					HandleEventGroupInviteReceived(eventData);
					return true;

				case ClientEventCode.GroupInviteCancelled:
					HandleEventGroupInviteCancelled(eventData);
					return true;

				case ClientEventCode.GroupInviteDeclined:
					HandleEventGroupInviteDeclined(eventData);
					return true;

				default:
					return false;
			}
		}

		/// <summary>
		/// Handles a(n) <see cref="OperationResponse"/>.
		/// </summary>
		bool IConnectionState.HandleOperationResponse(OperationResponse response)
		{
			switch ((ResultCode) response.ReturnCode)
			{
				case ResultCode.ChannelNotAvailable:
					{
						var messageType = (MessageType) response.Parameters[(byte) ParameterCode.MessageType];
						switch (messageType)
						{
							case MessageType.Group:
								{
									if (false == World.Player.InGroup)
									{
										World.Game.Chat.PostMessage(MessageType.Group, "You are not in a group");
									}
									else
									{
										World.Game.Chat.PostMessage(MessageType.Game, "Channel not available");
									}
								}
								break;

							case MessageType.Guild:
								World.Game.Chat.PostMessage(MessageType.Guild, "You are not in a guild");
								break;

							default:
								World.Game.Chat.PostMessage(MessageType.Game, "Channel not available");
								break;
						}
					}
					return true;

				default:
					{
						var errorMessage = ((ResultCode) response.ReturnCode).GetErrorMessage();
						World.Game.Chat.PostMessage(MessageType.Game, errorMessage);
					}
					return true;
			}
		}

		/// <summary>
		/// Sends an operation
		/// </summary>
		void IConnectionState.SendOperation(PhotonPeer peer, ClientOperationCode operationCode, Dictionary<byte, object> parameters, bool encrypt)
		{
			peer.OpCustom((byte) operationCode, parameters, true, 0, encrypt);
		}

		#endregion

		#region Event Handlers

		private void HandleEventObjectDestroyed(EventData eventData)
		{
			throw new NotImplementedException();
		}

		private void HandleEventObjectSubscribed(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];
			var pos = (float[])eventData[(byte)ParameterCode.Position];
			var orientation = (float)eventData[(byte)ParameterCode.Orientation];
			var position = new Vector3(pos[0], pos[1], pos[2]);

			var revision = 0;
			Hashtable serverProperties = null;

			object param;
			if (eventData.Parameters.TryGetValue((byte)ParameterCode.Revision, out param))
				revision = (int)param;

			if (eventData.Parameters.TryGetValue((byte)ParameterCode.Properties, out param))
				serverProperties = (Hashtable)param;

			switch ((ObjectType)guid.Type)
			{
				case ObjectType.Player:
					{
						var avatar = World.CreateOtherPlayerAvatar(guid, 1);
						var otherPlayer = new OtherPlayer(World, guid, avatar);

						if (World.AddMmoObject(otherPlayer))
						{
							otherPlayer.Initialize();
							otherPlayer.Spawn(position, new Vector3(0, orientation, 0));

							// if the server sent us properties just use it and skip loading from cache
							// the server will never send properties for players normally on subscribe
							if (serverProperties != null)
								otherPlayer.SetProperties(serverProperties, revision);
							else
							{
								// if the server did not send the properties (normally the case) check the storage for any cached properties
								System.Collections.Hashtable cachedProperties;
								if (World.ObjectDataStorage.TryGetCachedProperties(guid, out cachedProperties))
								{
									var oldRevision = (int)cachedProperties["revision"];
									cachedProperties.Remove("revision");

									// if the storage contains previous properties compare revisions
									// request new properties if the revisions are off
									// only request non-static properties since the static properties would have been stored in the cache already
									var flags = PropertyFlags.ActorNonStatic;
									if (revision > oldRevision && flags != PropertyFlags.None)
										Operations.GetProperties(guid, flags);

									// whatever the case set the properties with the old revision for now
									otherPlayer.SetProperties(cachedProperties, oldRevision);
									this.World.ObjectDataStorage.RemoveCachedProperties(guid);

									//Logger.DebugFormat(
									//	"{0}: Cached properties found. OldRevision: {1}. NewRevision: {2}. Requesting Non-Static Properties: {3}",
									//	actor.Name, oldRevision, revision, revision > oldRevision && flags != PropertyFlags.None);
								}
								else
								{
									// revision of 0 means the object does not have any properties
									var flags = PropertyFlags.ActorAll;
									if (revision > 0 && flags != PropertyFlags.None)
										Operations.GetProperties(guid, flags);

									//Logger.DebugFormat("{0}: Cached properties not found. NewRevision: {1}. Requesting All Properties: {2}",
									//				   actor.Name, revision, revision > 0 && flags != PropertyFlags.None);
								}
							}
						}
					}
					break;

				case ObjectType.Npc:
					{
						// loading global properties from local storage
						System.Collections.Hashtable storedProperties;
						if (false == World.ObjectDataStorage.TryGetStoredProperties(guid.SubId, out storedProperties))
							throw new InvalidDataException("MmoObjectPropertiesNotFound");

						var npcType = (NpcType)storedProperties[(byte)PropertyCode.NpcType];
						var instanceId = (npcType == NpcType.Enemy) ? 11 : 10;
						var avatar = World.CreateNpcAvatar(guid, instanceId);
						var npc = new Npc(World, guid, npcType, avatar);
						npc.SetProperties(storedProperties, revision);

						if (World.AddMmoObject(npc))
						{
							npc.Initialize();
							npc.Spawn(position, new Vector3(0, orientation, 0));

							// if the server sent us properties just use it and skip loading from cache
							// the server will never send properties for npcs normally
							if (serverProperties != null)
								npc.SetProperties(serverProperties, revision);
							else
							{
								// if the server did not send the properties (normally the case) check the storage for any cached properties
								System.Collections.Hashtable cachedProperties;
								if (World.ObjectDataStorage.TryGetCachedProperties(guid, out cachedProperties))
								{
									var oldRevision = (int)cachedProperties["revision"];
									cachedProperties.Remove("revision");

									// if the storage contains previous properties compare revisions
									// request new properties if the revisions are off
									// only request non-static properties since the static properties would have been stored in the cache already
									var flags = PropertyFlags.NpcNonStatic;
									if (revision > oldRevision && flags != PropertyFlags.None)
										Operations.GetProperties(guid, flags);

									// whatever the case set the properties with the old revision
									npc.SetProperties(cachedProperties, oldRevision);
									this.World.ObjectDataStorage.RemoveCachedProperties(guid);

									//Logger.DebugFormat(
									//	"{0}: Cached properties found. OldRevision: {1}. NewRevision: {2}. Requesting Non-Static Properties: {3}",
									//	npc.Name, oldRevision, revision, revision > oldRevision && flags != PropertyFlags.None);
								}
								else
								{
									// revision of 0 means the object does not have any properties
									var flags = PropertyFlags.NpcAll;
									if (revision > 0 && flags != PropertyFlags.None)
										Operations.GetProperties(guid, flags);

									//Logger.DebugFormat("{0}: Cached properties not found. NewRevision: {1}. Requesting All Properties: {2}",
									//				   npc.Name, revision, revision > 0 && flags != PropertyFlags.None);
								}
							}

							// flags are handled last since they may depend on the properties
							if (eventData.Parameters.TryGetValue((byte)ParameterCode.Flags, out param))
							{
								var flags = (InterestFlags)param;
								if (flags != InterestFlags.None)
									npc.SetInterestFlags(flags);
							}
						}
					}
					break;

				case ObjectType.Gameobject:
					{
						// loading global properties from local storage
						System.Collections.Hashtable storedProperties;
						if (false == World.ObjectDataStorage.TryGetStoredProperties(guid.SubId, out storedProperties))
							throw new InvalidDataException("MmoObjectPropertiesNotFound");

						var goType = (GameObjectType)storedProperties[(byte)PropertyCode.GoType];
						var instanceId = -guid.SubId;
						var avatar = World.CreateGameObjectAvatar(guid, instanceId);
						var go = new Gameobject(World, guid, goType, avatar);
						go.SetProperties(storedProperties, revision);

						if (World.AddMmoObject(go))
						{
							go.Initialize();
							go.Spawn(position, new Vector3(0, orientation, 0));

							// if the server sent us properties just use it and skip loading from cache
							// the server will never send properties for gameobjects normally
							if (serverProperties != null)
								go.SetProperties(serverProperties, revision);
							else
							{
								// if the server did not send the properties (normally the case) check the storage for any cached properties
								System.Collections.Hashtable cachedProperties;
								if (World.ObjectDataStorage.TryGetCachedProperties(guid, out cachedProperties))
								{
									var oldRevision = (int)cachedProperties["revision"];
									cachedProperties.Remove("revision");

									// if the storage contains previous properties compare revisions
									// request new properties if the revisions are off
									// only request non-static properties since the static properties would have been stored in the cache already
									var flags = PropertyFlags.GameobjectNonStatic;
									if (revision > oldRevision && flags != PropertyFlags.None)
										Operations.GetProperties(guid, flags);

									// whatever the case set the properties with the old revision
									go.SetProperties(cachedProperties, oldRevision);
									this.World.ObjectDataStorage.RemoveCachedProperties(guid);

									//Logger.DebugFormat(
									//	"{0}: Cached properties found. OldRevision: {1}. NewRevision: {2}. Requesting Non-Static Properties: {3}",
									//	go.Name, oldRevision, revision, revision > oldRevision && flags != PropertyFlags.None);
								}
								else
								{
									// revision of 0 means the object does not have any properties
									var flags = PropertyFlags.GameobjectAll;
									if (revision > 0 && flags != PropertyFlags.None)
										Operations.GetProperties(guid, flags);

									//Logger.DebugFormat("{0}: Cached properties not found. NewRevision: {1}. Requesting All Properties: {2}",
									//				   go.Name, revision, revision > 0 && flags != PropertyFlags.None);
								}
							}

							// flags are handled last since they may depend on the properties
							if (eventData.Parameters.TryGetValue((byte)ParameterCode.Flags, out param))
							{
								var flags = (InterestFlags)param;
								if (flags != InterestFlags.None)
									go.SetInterestFlags(flags);
							}
						}
					}
					break;

				case ObjectType.Dynamic:
					{
						if (serverProperties == null)
							throw new NullReferenceException("serverProperties");

						var dynamicType = (DynamicType)serverProperties[(byte)PropertyCode.DynamicType];
						var avatar = World.CreateDynamicAvatar(guid, guid.SubId);
						var dynamic = new Dynamic(World, guid, dynamicType, avatar);

						if (World.AddMmoObject(dynamic))
						{
							dynamic.Initialize();
							dynamic.Spawn(position, new Vector3(0, orientation, 0));
							dynamic.SetProperties(serverProperties, revision);
							// flags are handled last since they may depend on the properties
							if (eventData.Parameters.TryGetValue((byte)ParameterCode.Flags, out param))
							{
								var flags = (InterestFlags)param;
								if (flags != InterestFlags.None)
									dynamic.SetInterestFlags(flags);
							}
						}
					}
					break;
			}
		}

		private void HandlEventObjectUnsubscribed(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
			{
				mmoObject.Destroy();
				World.RemoveMmoObject(mmoObject.Guid);

				if (guid.Type == (byte)ObjectType.Player && World.Player.InGroup)
				{
					var member = World.Player.Group.GetMember(guid);
					if (member != null)
					{
						member.CopyLink();
						member.Link = null;
					}
				}
			}
			else
			{
				Debug.Log(string.Format("[HandlEventObjectUnsubscribed]: MmoObject (Id={0}) not found", guid));
			}
		}

		private void HandleEventObjectMovement(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
			{
				object value;
				if (eventData.Parameters.TryGetValue((byte)ParameterCode.Pitch, out value))
				{
					var rotation = mmoObject.Rotation;
					rotation.x = (float)value;
					mmoObject.Rotate(rotation);
				}

				if (eventData.Parameters.TryGetValue((byte)ParameterCode.Orientation, out value))
				{
					var rotation = mmoObject.Rotation;
					rotation.y = (float)value;
					mmoObject.Rotate(rotation);
				}

				if (eventData.Parameters.TryGetValue((byte)ParameterCode.MovementDirection, out value))
					mmoObject.SetDirection((MovementDirection)value);

				if (eventData.Parameters.TryGetValue((byte)ParameterCode.MovementState, out value))
					mmoObject.SetMovementState((MovementState)value);

				if (eventData.Parameters.TryGetValue((byte)ParameterCode.Speed, out value))
					mmoObject.SetSpeed((byte)value);
			}
			else
			{
				Debug.Log(string.Format("[HandleEventObjectMovement]: (Id={0}) not found", guid));
			}
		}

		private void HandleEventObjectTransform(EventData eventData)
		{
			var guid = eventData[(byte)ParameterCode.ObjectId];
			if (guid != null)
			{
				MmoObject mmoObject;
				if (World.TryGetMmoObject((long)guid, out mmoObject))
				{
					object value;
					if (eventData.Parameters.TryGetValue((byte)ParameterCode.Pitch, out value))
					{
						var rotation = mmoObject.Rotation;
						rotation.x = (float)value;
						mmoObject.Rotate(rotation);
					}

					if (eventData.Parameters.TryGetValue((byte)ParameterCode.Orientation, out value))
					{
						var rotation = mmoObject.Rotation;
						rotation.y = (float)value;
						mmoObject.Rotate(rotation);
					}

					if (eventData.Parameters.TryGetValue((byte)ParameterCode.Position, out value))
						mmoObject.Move((value as float[]).ToVector());
				}
				else
				{
					Debug.Log(string.Format("[HandleEventObjectTransform]: (Id={0}) not found", guid));
				}
			}
			else
			{
				object value;
				if (eventData.Parameters.TryGetValue((byte)ParameterCode.Position, out value))
					World.Player.Move((value as float[]).ToVector());
			}
		}

		private void HandleEventObjectProperty(EventData eventData)
		{
			var guid = eventData[(byte)ParameterCode.ObjectId];
			var propertiesCode = (PropertyCode)eventData[(byte)ParameterCode.PropertyCode];
			var value = eventData[(byte)ParameterCode.Data];

			MmoObject mmoObject = World.Player;
			if (guid != null)
				World.TryGetMmoObject((long)guid, out mmoObject);

			if (mmoObject != null)
				mmoObject.SetProperty(propertiesCode, value);
		}

		private void HandleEventObjectPropertyMultiple(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];
			var properties = eventData[(byte)ParameterCode.Properties] as Hashtable;
			var revision = (int)eventData[(byte)ParameterCode.Revision];

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
			{
				mmoObject.SetProperties(properties, revision);
				if (guid == World.Player.Guid)
					return;

				if (guid.Type == (byte)ObjectType.Player && World.Player.InGroup)
				{
					var member = World.Player.Group.GetMember(guid);
					if (member != null)
					{
						member.Link = mmoObject as OtherPlayer;
					}
				}
			}
			else
			{
				if (guid == World.Player.Guid)
				{
					World.Player.SetProperties(properties, revision);
				}
				else
				{
					Debug.Log(string.Format("[HandleEventObjectPropertyMultiple]: MmoObject (Id={0}) not found", guid));
				}
			}
		}

		private void HandleEventObjectFlagsSet(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
			{
				var flags = (InterestFlags) eventData[(byte) ParameterCode.Flags];
				mmoObject.SetInterestFlags(flags);
			}
			else
			{
				Debug.Log(string.Format("[HandleEventObjectFlagsSet]: MmoObject (Id={0}) not found", guid));
			}
		}

		private void HandleEventObjectFlagsUnset(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
			{
				var flags = (InterestFlags)eventData[(byte)ParameterCode.Flags];
				mmoObject.UnsetInterestFlags(flags);
			}
			else
			{
				Debug.Log(string.Format("[HandleEventObjectFlagsUnset]: MmoObject (Id={0}) not found", guid));
			}
		}

		private void HandleEventObjectLevelUp(EventData eventData)
		{
			var guid = (long) eventData[(byte) ParameterCode.ObjectId];

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
				((OtherPlayer) mmoObject).LevelUp();
		}

		private void HandleEventYouLevelUp(EventData eventData)
		{
			var xp = (int) eventData[(byte) ParameterCode.Xp];
			World.Player.LevelUp();
			World.Player.SetXp(xp);
		}

		private void HandleEventInteractionShop(EventData eventData)
		{
			MmoGuid guid = (long) eventData[(byte) ParameterCode.ObjectId];
			// if the guids don't match this may mean the client tried to interact with another object
			// before the origianl interaction event was received (a delayed event)
			if (World.Player.CurrentInteractor.Guid != guid)
				return;

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
			{
				var npc = (Npc) mmoObject;
				if (npc.Inventory == null)
					return;

				// displaying the shop
				WindowManager.Instance.ShowMerchantWindow(npc.Inventory, npc, World.Player.CloseInteraction);
			}
		}

		private void HandleEventInteractionShopList(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];
			var items = eventData[(byte)ParameterCode.Collection] as short[];

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
			{
				var npc = (Npc) mmoObject;
				// setting the inventory
				npc.SetInventory(items);
				// if the guids don't match this may mean the client tried to interact with another object
				// before the origianl interaction event was received (a delayed event)
				if (World.Player.CurrentInteractor.Guid != guid)
					return;

				// displaying the shop
				// re-acquire the items explictely because the object may skip or handle items differently
				WindowManager.Instance.ShowMerchantWindow(npc.Inventory, mmoObject, World.Player.CloseInteraction);
			}
		}

		private void HandleEventInteractionLoot(EventData eventData)
		{
			MmoGuid guid = (long) eventData[(byte) ParameterCode.ObjectId];
			// if the guids don't match this may mean the client tried to interact with another object
			// before the origianl interaction event was received (a delayed event)
			if (World.Player.CurrentInteractor.Guid != guid)
				return;

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
			{
				if (!mmoObject.HaveLoot())
					return;

				// displaying the loot
				WindowManager.Instance.ShowLootWindow(mmoObject.GetLoot(), mmoObject, World.Player.CloseInteraction);
			}
		}

		private void HandleEventInteractionLootList(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];
			var gold = eventData[(byte)ParameterCode.Gold];
			var items = eventData[(byte)ParameterCode.Collection] as ContainerItemStructure[];

			LootItem[] lootItems = null;
			if (items != null)
			{
				lootItems = new LootItem[items.Length];
				for (var i = 0; i < items.Length; i++)
				{
					var item = items[i];
					lootItems[i] = new LootItem(item.ItemId, item.Index, item.Count);

					MmoItemData itemData;
					if (MmoItemDataStorage.Instance.TryGetMmoItem(item.ItemId, out itemData))
						lootItems[i].Item = itemData;
				}
			}
			
			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
			{
				// setting the loot
				mmoObject.SetLoot(new Loot((int) (gold ?? 0), lootItems));
				// if the guids don't match this may mean the client tried to interact with another object
				// before the origianl interaction event was received (a delayed event)
				if (World.Player.CurrentInteractor.Guid != guid)
					return;

				// displaying the loot
				// re-acquire the loot explictely because the object may skip or handle loots differently
				WindowManager.Instance.ShowLootWindow(mmoObject.GetLoot(), mmoObject, World.Player.CloseInteraction);
			}
		}

		private void HandleEventInteractionDialogue(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];
			// if the guids don't match this may mean the client tried to interact with another object
			// before the origianl interaction event was received (a delayed event)
			if (World.Player.CurrentInteractor.Guid != guid)
				return;

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
			{
				var npc = (Npc)mmoObject;
				if (npc.Dialogue == null)
					return;

				// loading the intro message for the npc
				var intro = GameResources.Instance.LoadMessage(npc.IntroMessageId, 0);
				// displaying the dialogue
				WindowManager.Instance.ShowDialogueMenuWindow(npc.Dialogue, intro, npc, World.Player.CloseInteraction);
			}
		}

		private void HandleEventInteractionDialogueList(EventData eventData)
		{
			MmoGuid guid = (long) eventData[(byte) ParameterCode.ObjectId];
			var menuItems = eventData[(byte) ParameterCode.Collection] as MenuItemStructure[];
			
			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
			{
				var npc = (Npc) mmoObject;
				// setting the dialogue
				npc.SetDialogue(menuItems);
				// if the guids don't match this may mean the client tried to interact with another object
				// before the origianl interaction event was received (a delayed event)
				if (World.Player.CurrentInteractor.Guid != guid)
					return;

				// loading the intro message for the npc
				var intro = GameResources.Instance.LoadMessage(npc.IntroMessageId, 0);
				// displaying the dialogue
				WindowManager.Instance.ShowDialogueMenuWindow(npc.Dialogue, intro, npc, World.Player.CloseInteraction);
			}
		}

		private void HandleEventInventoryInit(EventData eventData)
		{
			var size = (byte) eventData[(byte) ParameterCode.Size];
			var items = eventData[(byte) ParameterCode.Collection] as ContainerItemStructure[];

			var inventory = World.Player.Inventory;
			inventory.Resize(size);

			if (items != null)
				foreach (var itemData in items)
					inventory.AddItemAt(itemData.ItemId, itemData.Index, itemData.Count);
		}

		private void HandleEventInventoryItemAdded(EventData eventData)
		{
			var itemInfo = (ContainerItemStructure)eventData[(byte)ParameterCode.Data];
			World.Player.Inventory.AddItemAt(itemInfo.ItemId, itemInfo.Index, itemInfo.Count);
		}

		private void HandleEventInventoryItemAddedMultiple(EventData eventData)
		{
			var items = (ContainerItemStructure[])eventData[(byte)ParameterCode.Collection];
			foreach (var itemData in items)
				World.Player.Inventory.AddItemAt(itemData.ItemId, itemData.Index, itemData.Count);
		}

		private void HandleEventInventoryItemRemoved(EventData eventData)
		{
			var index = (byte) eventData[(byte) ParameterCode.Index];
			var quantity = (byte)eventData[(byte)ParameterCode.Quantity];

			World.Player.Inventory.RemoveItemAt(index, quantity);
		}

		private void HandleEventInventoryItemMoved(EventData eventData)
		{
			var indexFrom = (byte) eventData[(byte) ParameterCode.IndexFrom];
			var indexTo = (byte) eventData[(byte) ParameterCode.Index];

			World.Player.Inventory.MoveItem(indexTo, indexFrom);
		}

		private void HandleEventActionBarInit(EventData eventData)
		{
			var actionItems = eventData[(byte)ParameterCode.Collection] as ActionItemStructure[];
			if (actionItems != null)
				foreach (var actionItemData in actionItems)
					this.World.Player.Actionbar.AddItemAt((ActionItemType) actionItemData.Type, actionItemData.ItemId, actionItemData.Index);
		}

		private void HandleEventSpellManagerInit(EventData eventData)
		{
			var spellIds = (short[]) eventData[(byte) ParameterCode.Collection];
			foreach (var spellId in spellIds)
				World.Player.SpellManager.AddSpell(spellId);
		}

		private void HandleEventSpellAdded(EventData eventData)
		{
			var spellId = (short)eventData[(byte)ParameterCode.SpellId];
			World.Player.SpellManager.AddSpell(spellId);
		}

		private void HandleEventSpellRemoved(EventData eventData)
		{
			var spellId = (short)eventData[(byte)ParameterCode.SpellId];
			World.Player.SpellManager.RemoveSpell(spellId);
		}

		private void HandleEventSpellCastBegin(EventData eventData)
		{
			var spellId = (short)eventData[(byte)ParameterCode.SpellId];
			if (spellId == GameSettings.USE_SPELL_ID)
			{
				World.Player.SpellManager.BeginUse();
			}
			else
			{
				World.Player.SpellManager.BeginCast(spellId);
			}
		}

		private void HandleEventSpellCastEnd(EventData eventData)
		{
			World.Player.SpellManager.EndCast();
		}

		private void HandleEventSpellCooldownBegin(EventData eventData)
		{
			var spellId = (short)eventData[(byte)ParameterCode.SpellId];
			if(spellId == -1)
			{
				World.Player.SpellManager.BeginGCD();
			}
			else
			{
				Spell spell;
				if (World.Player.SpellManager.TryGetSpell(spellId, out spell))
					spell.BeginCooldown();
			}
		}

		private void HandleEventSpellCooldownEnd(EventData eventData)
		{
			var spellId = (short)eventData[(byte)ParameterCode.SpellId];
			if(spellId == -1)
			{
				World.Player.SpellManager.EndGCD();
			}
			else
			{
				Spell spell;
				if (World.Player.SpellManager.TryGetSpell(spellId, out spell))
					spell.EndCooldown();
			}
		}

		private void HandleEventLootInit(EventData eventData)
		{
			MmoGuid guid = (long) eventData[(byte) ParameterCode.ObjectId];
			var gold = eventData[(byte) ParameterCode.Gold];
			var items = eventData[(byte) ParameterCode.Collection] as int[];

			LootItem[] lootItems = null;
			if (items != null)
			{
				lootItems = new LootItem[items.Length];
				for (var i = 0; i < items.Length; i++)
				{
					MmoItemData itemData;
					if (MmoItemDataStorage.Instance.TryGetMmoItem(lootItems[i].ItemId, out itemData))
						lootItems[i].Item = itemData;
				}
			}

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
				mmoObject.SetLoot(new Loot((int) (gold ?? 0), lootItems));
		}

		private void HandleEventLootClear(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
				mmoObject.SetLoot(null);
		}

		private void HandleEventLootItemRemoved(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];
			var index = (byte) eventData[(byte) ParameterCode.Index];

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
				mmoObject.GetLoot().RemoveLootItem(index);
		}

		private void HandleEventLootGoldRemoved(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
				mmoObject.GetLoot().RemoveGold();
		}

		private void HandleEventQuestManagerInit(EventData eventData)
		{
			var quests = (ActiveQuestStructure[]) eventData[(byte) ParameterCode.Collection];
			foreach (var questInfo in quests)
				World.Player.QuestManager.AddQuest(questInfo.QuestId, (QuestStatus) questInfo.Status, questInfo.Counts);
		}

		private void HandleEventQuestGiverStatus(EventData eventData)
		{
			MmoGuid guid = (long) eventData[(byte) ParameterCode.ObjectId];
			var status = (QuestPickupStatus) eventData[(byte) ParameterCode.Status];

			MmoObject mmoObject;
			if (World.TryGetMmoObject((long) guid, out mmoObject))
				mmoObject.SetQuestPickupStatus(status);
		}

		private void HandleEventQuestStarted(EventData eventData)
		{
			var questId = (short) eventData[(byte) ParameterCode.QuestId];
			World.Player.QuestManager.StartQuest(questId);
		}

		private void HandleEventQuestFinished(EventData eventData)
		{
			var questId = (short)eventData[(byte)ParameterCode.QuestId];
			World.Player.QuestManager.FinishQuest(questId);
		}

		private void HandleEventQuestProgress(EventData eventData)
		{
			var questProgressInfo = (QuestProgressStructure) eventData[(byte) ParameterCode.Data];
			World.Player.QuestManager.ProgressQuest(questProgressInfo.QuestId, questProgressInfo.Index, questProgressInfo.Count);
		}

		private void HandleEventQuestRegress(EventData eventData)
		{
			throw new NotImplementedException();
		}

		private void HandleEventQuestAdded(EventData eventData)
		{
			throw new NotImplementedException();
		}

		private void HandleEventChatChannelJoined(EventData eventData)
		{
			var channelType = (ChannelType)eventData[(byte)ParameterCode.ChannelType];
			var channelName = string.Empty;

			object param;
			if (eventData.Parameters.TryGetValue((byte)ParameterCode.ChannelName, out param))
				channelName = (string)param;

			ChatManager.Instance.OnJoinedChannel(channelType, channelName);
		}

		private void HandleEventChatChannelLeft(EventData eventData)
		{
			var channelType = (ChannelType)eventData[(byte)ParameterCode.ChannelType];
			var channelName = string.Empty;

			object param;
			if (eventData.Parameters.TryGetValue((byte)ParameterCode.ChannelName, out param))
				channelName = (string)param;

			ChatManager.Instance.OnLeftChannel(channelType, channelName);
		}

		private void HandleEventChatMessageReceived(EventData eventData)
		{
			var message = (string)eventData[(byte)ParameterCode.Message];
			var messageType = (MessageType)eventData[(byte)ParameterCode.MessageType];

			object param;
			if (eventData.Parameters.TryGetValue((byte)ParameterCode.Sender, out param))
			{
				var sender = (string)param;
				if (eventData.Parameters.TryGetValue((byte)ParameterCode.Receiver, out param))
				{
					var receiver = (string)param;
					ChatManager.Instance.PostMessage(messageType, message, sender, receiver);
				}
				else
				{
					ChatManager.Instance.PostMessage(messageType, message, sender);
				}
			}
			else
			{
				ChatManager.Instance.PostMessage(messageType, message);
			}
		}

		private void HandleEventSocialFriendAddedName(EventData eventData)
		{
			var nameOfProfile = (string) eventData[(byte) ParameterCode.CharacterName];
			World.Player.SocialManager.AddFriend(nameOfProfile);
		}

		private void HandleEventSocialFriendAddedNameMultiple(EventData eventData)
		{
			var namesOfProfiles = (string[]) eventData[(byte) ParameterCode.Collection];
			foreach (var nameOfProfile in namesOfProfiles)
				World.Player.SocialManager.AddFriend(nameOfProfile);
		}

		private void HandleEventSocialFriendAddedData(EventData eventData)
		{
			var profileInfo = (ProfileStructure)eventData[(byte)ParameterCode.Data];
			World.Player.SocialManager.AddFriend(profileInfo);
		}

		private void HandleEventSocialFriendAddedDataMultiple(EventData eventData)
		{
			var profileInfos = (ProfileStructure[])eventData[(byte)ParameterCode.Collection];
			foreach (var friendInfo in profileInfos)
				World.Player.SocialManager.AddFriend(friendInfo);
		}

		private void HandleEventSocialIgnoreAddedName(EventData eventData)
		{
			throw new NotImplementedException();
		}

		private void HandleEventSocialIgnoreAddedNameMultiple(EventData eventData)
		{
			throw new NotImplementedException();
		}

		private void HandleEventSocialProfileUpdate(EventData eventData)
		{
			var profileInfo = (ProfileStructure)eventData[(byte)ParameterCode.Data];
			World.Player.SocialManager.UpdateFriend(profileInfo.Name, profileInfo);
		}

		private void HandleEventSocialProfileUpdateMultiple(EventData eventData)
		{
			var profileInfos = (ProfileStructure[])eventData[(byte)ParameterCode.Collection];
			foreach (var profileInfo in profileInfos)
				World.Player.SocialManager.UpdateFriend(profileInfo.Name, profileInfo);
		}

		private void HandleEventSocialProfileRemoved(EventData eventData)
		{
			var nameOfProfile = (string)eventData[(byte)ParameterCode.CharacterName];
			World.Player.SocialManager.RemoveFriend(nameOfProfile);
		}
		 
		private void HandleEventSocialProfileStatus(EventData eventData)
		{
			var nameOfProfile = (string)eventData[(byte)ParameterCode.CharacterName];
			var status = (byte)eventData[(byte)ParameterCode.Status];
			World.Player.SocialManager.UpdateStatus(nameOfProfile, (SocialStatus)status);
		}

		private void HandleEventSocialFriendRequestReceived(EventData eventData)
		{
			var sender = (string)eventData[(byte)ParameterCode.Sender];
			World.Player.SocialManager.ReceiveFriendRequest(sender);
		}

		private void HandleEventSocialFriendRequestCancelled(EventData eventData)
		{
			var sender = (string)eventData[(byte)ParameterCode.Sender];
			World.Player.SocialManager.CancelFriendRequest(sender);
		}

		private void HandleEventGroupInit(EventData eventData)
		{
			var groupInfo = (GroupStructure)eventData[(byte)ParameterCode.Data];
			World.Player.Group = new Group(groupInfo.GroupId, groupInfo.LeaderId);
		}

		private void HandleEventGroupMemberAdded(EventData eventData)
		{
			var memberInfo = (GroupMemberStructure) eventData[(byte) ParameterCode.Data];
			var member = new GroupMember(memberInfo.Guid)
				{
					Name = memberInfo.Name,
					MaxHp = 1,
					CurrHp = 0,
					Status = GroupMemberStatus.Online
				};

			MmoObject mmoObject;
			if (World.TryGetMmoObject(memberInfo.Guid, out mmoObject))
				member.Link = (OtherPlayer) mmoObject;

			World.Player.Group.AddMember(member);
		}

		private void HandleEventGroupMemberAddedGuid(EventData eventData)
		{
			MmoGuid guid = (long) eventData[(byte) ParameterCode.ObjectId];
			var member = new GroupMember(guid);

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
				member.Link = (OtherPlayer) mmoObject;

			World.Player.Group.AddMember(member);
		}

		private void HandleEventGroupMemberAddedProperties(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];
			var properties = (Hashtable)eventData[(byte)ParameterCode.Data];

			var member = new GroupMember(guid);
			member.SetProperties(properties);

			MmoObject mmoObject;
			if (World.TryGetMmoObject(guid, out mmoObject))
				member.Link = (OtherPlayer)mmoObject;

			World.Player.Group.AddMember(member);
		}

		private void HandleEventGroupMemberAddedInactive(EventData eventData)
		{
			var memberInfo = (GroupMemberStructure)eventData[(byte)ParameterCode.Data];
			var member = new GroupMember(memberInfo.Guid)
				{
					Name = memberInfo.Name,
					MaxHp = 1,
					CurrHp = 0,
					Status = GroupMemberStatus.Offline
				};

			World.Player.Group.AddMember(member);
		}

		private void HandleEventGroupMemberRemoved(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];
			var member = World.Player.Group.GetMember(guid);

			if (member != null)
			{
				World.Player.Group.RemoveMember(guid);
				ChatManager.Instance.PostMessage(MessageType.Group, member.Name + " has left the group");
			}
		}

		private void HandleEventGroupMemberUpdate(EventData eventData)
		{
			MmoGuid guid = (long)eventData[(byte)ParameterCode.ObjectId];
			var properties = (Hashtable)eventData[(byte)ParameterCode.Data];

			var member = World.Player.Group.GetMember(guid);
			if (member != null)
				member.SetProperties(properties);
		}

		private void HandleEventGroupMemberDisconnected(EventData eventData)
		{
			MmoGuid guid = (long) eventData[(byte) ParameterCode.ObjectId];

			var member = World.Player.Group.GetMember(guid);
			if (member != null)
				member.Status = GroupMemberStatus.Offline;
		}

		private void HandleEventGroupUninvited(EventData eventData)
		{
			World.Player.Group = null;
			ChatManager.Instance.PostMessage(MessageType.Group, "You have left the group.");
		}

		private void HandleEventGroupDisbanded(EventData eventData)
		{
			World.Player.Group = null;
			ChatManager.Instance.PostMessage(MessageType.Group, "Your group has been disbanded.");
		}

		private void HandleEventGroupInviteReceived(EventData eventData)
		{
			var inviter = (string) eventData[(byte)ParameterCode.Sender];
			var message = inviter.CapFirstLetter() + " has invited you to a group. Would you like to join?";

			HUD.Instance.ShowAcceptDeclineBox(message, Operations.AcceptGroupInvite, Operations.DeclineGroupInvite);
		}

		private void HandleEventGroupInviteCancelled(EventData eventData)
		{
			throw new NotImplementedException();
		}

		private void HandleEventGroupInviteDeclined(EventData eventData)
		{
			var invited = (string) eventData[(byte)ParameterCode.Receiver];
			ChatManager.Instance.PostMessage(MessageType.Group, invited.CapFirstLetter() + " has declined your invitation");

			var group = World.Player.Group;
			if (group != null && group.Count == 0)
				World.Player.Group = null;
		}

		#endregion
	}
}
