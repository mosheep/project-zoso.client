﻿using ExitGames.Client.Photon;

using Karen90MmoFramework.Game;

namespace Karen90MmoFramework.Client
{
	public static class TypeSerializer
	{
		public static void RegisterTypes()
		{
			PhotonPeer.RegisterType(typeof(CharacterStructure), (byte)CustomTypeCode.CharacterStructure,
									GlobalSerializerMethods.SerializeCharacterStructure,
									GlobalSerializerMethods.DeserializeCharacterStructure);

			PhotonPeer.RegisterType(typeof(ItemStructure), (byte)CustomTypeCode.ItemStructure,
									 GlobalSerializerMethods.SerializeItemStructure,
									 GlobalSerializerMethods.DeserializeItemStructure);

			PhotonPeer.RegisterType(typeof(ContainerItemStructure), (byte)CustomTypeCode.ContainerItemStructure,
									 GlobalSerializerMethods.SerializeContainerItemStructure,
									 GlobalSerializerMethods.DeserializeContainerItemStructure);

			PhotonPeer.RegisterType(typeof(ActionItemStructure), (byte)CustomTypeCode.ActionItemStructure,
									 GlobalSerializerMethods.SerializeActionItemStructure,
									 GlobalSerializerMethods.DeserializeActionItemStructure);

			PhotonPeer.RegisterType(typeof(SlotItemStructure), (byte)CustomTypeCode.SlotItemStructure,
									 GlobalSerializerMethods.SerializeSlotItemStructure,
									 GlobalSerializerMethods.DeserializeSlotItemStructure);

			PhotonPeer.RegisterType(typeof(MenuItemStructure), (byte)CustomTypeCode.MenuItemStructure,
									 GlobalSerializerMethods.SerializeMenuItemStructure,
									 GlobalSerializerMethods.DeserializeMenuItemStructure);

			PhotonPeer.RegisterType(typeof(ActiveQuestStructure), (byte)CustomTypeCode.ActiveQuestStructure,
									GlobalSerializerMethods.SerializeActiveQuestStructure,
									GlobalSerializerMethods.DeserializeActiveQuestStructure);

			PhotonPeer.RegisterType(typeof(QuestProgressStructure), (byte)CustomTypeCode.QuestProgressStructure,
									GlobalSerializerMethods.SerializeQuestProgressStructure,
									GlobalSerializerMethods.DeserializeQuestProgressStructure);

			PhotonPeer.RegisterType(typeof(GroupStructure), (byte)CustomTypeCode.GroupStructure,
									 GlobalSerializerMethods.SerializeGroupStructure,
									 GlobalSerializerMethods.DeserializeGroupStructure);

			PhotonPeer.RegisterType(typeof(GroupMemberStructure), (byte)CustomTypeCode.GroupMemberStructure,
									 GlobalSerializerMethods.SerializeGroupMemberStructure,
									 GlobalSerializerMethods.DeserializeGroupMemberStructure);

			PhotonPeer.RegisterType(typeof(ProfileStructure), (byte)CustomTypeCode.ProfileStructure,
									 GlobalSerializerMethods.SerializeProfileStructure,
									 GlobalSerializerMethods.DeserializeProfileStructure);
		}
	}
}
