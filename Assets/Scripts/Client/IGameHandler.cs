﻿namespace Karen90MmoFramework.Client
{
	public interface IGameHandler
	{
		/// <summary>
		/// Initializes the handler
		/// </summary>
		void Initialize();

		/// <summary>
		/// Updates the handler
		/// </summary>
		void Update();

		/// <summary>
		/// Performs drawing the GUI
		/// </summary>
		void Draw();

		/// <summary>
		/// Called after all updates have finished
		/// </summary>
		void LateUpdate();

		/// <summary>
		/// Destroys the handler
		/// </summary>
		void Destroy(DestroyReason destroyReason);
	}
}
