﻿using System;
using UnityEngine;
using ExitGames.Client.Photon;

using Karen90MmoFramework.Rpc;
using Karen90MmoFramework.Client;

public sealed class GameManager : MonoBehaviour, IConnectResponseHandler
{
	#region Constants and Fields

	/// <summary>
	/// the current game handler
	/// </summary>
	private IGameHandler currentGameHandler;

	/// <summary>
	/// the connection
	/// </summary>
	private IConnection connection;

	/// <summary>
	/// called before destroyed
	/// </summary>
	private event Action onDispose;

	/// <summary>
	/// called on the start of every frame
	/// </summary>
	private event Action onNextFrame;

	#endregion

	#region Properties

	/// <summary>
	/// Gets the singleton instance
	/// </summary>
	public static GameManager Instance { get; private set; }

	/// <summary>
	/// Gets the gui settings
	/// </summary>
	public GUISettings GuiSettings { get; private set; }

	/// <summary>
	/// Gets the server peer
	/// </summary>
	public NetworkEngine Network { get; private set; }

	/// <summary>
	/// Gets the message registerer
	/// </summary>
	public IMessageDispatcher MessageDispatcher { get; private set; }

	/// <summary>
	/// Gets the connection state
	/// </summary>
	public PeerStateValue ConnectionState
	{
		get
		{
			return connection.PeerState;
		}
	}

	#endregion

	#region Implementation of MonoBehvariour

	// ReSharper disable UnusedMember.Local
	void Awake()
	{
		Instance = this;
		Application.runInBackground = true;

		this.GuiSettings = GameObject.Find("[GameSettings]").GetComponent<GUISettings>();
		this.Network = NetworkEngine.Instance;
		this.MessageDispatcher = NetworkEngine.Instance;
		this.connection = NetworkEngine.Instance;
		
		// registering login events
		this.onDispose += MessageDispatcher.RegisterEventHandler((byte) ClientEventCode.UserLoggedIn, this.HandleEventUserLoggedIn).Dispose;
		this.onDispose += MessageDispatcher.RegisterEventHandler((byte) ClientEventCode.CharacterLoggedIn, this.HandleEventCharacterLoggedIn).Dispose;

		this.SetCurrentGameHandler(new UserLoginHandler(this));
	}

	void Update()
	{
		if (onNextFrame != null)
			this.onNextFrame();

		this.connection.Update();
		this.currentGameHandler.Update();

		this.onNextFrame = null;
	}

	void OnGUI()
	{
		GUI.skin = GuiSettings.DefaultSkin;
		this.currentGameHandler.Draw();
	}

	void LateUpdate()
	{
		this.currentGameHandler.LateUpdate();
	}

	void OnApplicationQuit()
	{
		this.currentGameHandler.Destroy(DestroyReason.AppExit);
		this.connection.Disconnect();
	}
	// ReSharper restore UnusedMember.Local

	#endregion

	#region Public Methods

	/// <summary>
	/// Executes a method at the start of the next frame
	/// </summary>
	/// <param name="method"></param>
	public void ExecuteAtNextFrame(Action method)
	{
		this.onNextFrame += method;
	}

	/// <summary>
	/// Sets the current game handler
	/// </summary>
	public void SetCurrentGameHandler(IGameHandler gameHandler)
	{
		this.DoSetCurrentGameHandler(gameHandler, true);
	}

	/// <summary>
	/// Sets the current game handler
	/// </summary>
	void DoSetCurrentGameHandler(IGameHandler gameHandler, bool destroyOld)
	{
		if (gameHandler == null)
			throw new NullReferenceException();

		if (destroyOld && currentGameHandler != null)
			this.currentGameHandler.Destroy(DestroyReason.HandlerChanged);

		foreach (var objectToDestroy in FindObjectsOfType<GamePersister>())
			objectToDestroy.Destroy();

		this.currentGameHandler = gameHandler;
		this.currentGameHandler.Initialize();
	}

	/// <summary>
	/// Connects to the server
	/// </summary>
	/// <param name="connectResponseHandler"></param>
	public void Connect(IConnectResponseHandler connectResponseHandler)
	{
		switch (connection.PeerState)
		{
			case PeerStateValue.Connecting:
			case PeerStateValue.Connected:
				Debug.LogWarning(string.Format("Connection state (State={0}) is invalid", connection.PeerState));
				return;
		}

		this.explicitConnectResponseHandler = connectResponseHandler;
		this.connection.Connect(this);
	}

	/// <summary>
	/// Disconnects the server connection
	/// </summary>
	public void Disconnect()
	{
		switch (connection.PeerState)
		{
			case PeerStateValue.Disconnecting:
			case PeerStateValue.Disconnected:
				Debug.LogWarning(string.Format("Connection state (State={0}) is invalid", connection.PeerState));
				return;
		}

		this.connection.Disconnect();
	}

	#endregion

	#region Implementation of IConnectResponseHandler

	private IConnectResponseHandler explicitConnectResponseHandler;
	
	/// <summary>
	/// Called when the server connection has been established
	/// </summary>
	void IConnectResponseHandler.OnConnected()
	{
		if(explicitConnectResponseHandler != null)
			this.explicitConnectResponseHandler.OnConnected();
		this.explicitConnectResponseHandler = null;
	}

	/// <summary>
	/// Called when the server connection has been destroyed
	/// </summary>
	void IConnectResponseHandler.OnDisconnected()
	{
		if(explicitConnectResponseHandler != null)
			this.explicitConnectResponseHandler.OnDisconnected();
		this.explicitConnectResponseHandler = null;

		if (currentGameHandler != null)
		{
			if (currentGameHandler is UserLoginHandler)
				return;
			this.currentGameHandler.Destroy(DestroyReason.Disconnect);
		}

		if (onDispose != null)
			onDispose();

		foreach (var objectToDestroy in FindObjectsOfType<SystemPersister>())
			objectToDestroy.Destroy();
		
		Application.LoadLevel(0);
		//this.DoSetCurrentGameHandler(new UserLoginHandler(this), false);
	}

	#endregion

	#region Event Handlers

	/// <summary>
	/// Sets the current game handler to <see cref="CharacterLoginHandler"/>.
	/// </summary>
	/// <param name="eventData"></param>
	void HandleEventUserLoggedIn(EventData eventData)
	{
		this.SetCurrentGameHandler(new CharacterLoginHandler(this));
	}

	/// <summary>
	/// Sets the current game handler to <see cref="GameHandler"/>.
	/// </summary>
	/// <param name="eventData"></param>
	void HandleEventCharacterLoggedIn(EventData eventData)
	{
		this.SetCurrentGameHandler(new GameHandler(this));
	}

	#endregion
};
