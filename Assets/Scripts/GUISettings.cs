﻿using UnityEngine;
using Karen90MmoFramework.Game;

// ReSharper disable InconsistentNaming
// ReSharper disable CheckNamespace
public class GUISettings : MonoBehaviour
{
	public LayerMask ClickLayers;

	public GUIStyle GUIStyleButtonClose;
	public GUIStyle GUIStyleButtonNpcChoiceList;
	public GUIStyle GUIStyleButtonSelection;
	public GUIStyle GUIStyleButtonPlain;
	public GUIStyle GUIStyleButtonContextMenu;
	public GUIStyle GUIStyleButtonTabs;
	public GUIStyle GUIStyleButtonLootName;
	public GUIStyle GUIStyleButtonQuestReward;
	public GUIStyle GUIStyleButtonMinimapOption;
	public GUIStyle GUIStyleButtonLink;
	public GUIStyle GUIStyleButtonCharacterSelection;
	public GUIStyle GUIStyleChatLink;
	public GUIStyle GUIStyleButtonAccept;
	public GUIStyle GUIStyleButtonDecline;
	
	public GUIStyle GUIStyleCharacterAttributesInfoName;
	public GUIStyle GUIStyleCharacterStatValue;
	public GUIStyle GUIStyleCharacterSpellItemName;

	public GUIStyle GUIStyleCastBarPlayerAbilityName;
	public GUIStyle GUIStyleCastBarPlayerAbilityTimer;

	public GUIStyle GUIStyleLabel;
	public GUIStyle GUIStyleLabelChatMessage;
	public GUIStyle GUIStyleLabelCharacterSelected;
	public GUIStyle GUIStyleLabelCenterTrajen;
	public GUIStyle GUIStyleLabelNameplate;
	public GUIStyle GUIStyleLabelFocusName;
	public GUIStyle GUIStyleLabelGroupMemberName;
	public GUIStyle GUIStyleLabelInventoryLeft;
	public GUIStyle GUIStyleLabelInventoryRight;
	public GUIStyle GUIStyleLabelQuestSubTitle;
	public GUIStyle GUIStyleLabelTrackerQuestName;
	public GUIStyle GUIStyleLabelTrackerObjective;
	public GUIStyle GUIStyleLabelHover;
	public GUIStyle GUIStyleLabelStackCount;
	public GUIStyle GUIStyleLabelPlayerName;
	public GUIStyle GUIStyleLabelDescription;
	public GUIStyle GUIStyleLabelNumericNotification;
	public GUIStyle GUIStyleLabelFriendRequest;

	public GUIStyle GUIStyleOptionCreation;

	public GUIStyle GUIStyleTextFieldChat;

	public GUIStyle GUIStyleMerchantItemName;
	public GUIStyle GUIStyleMerchantItemPrice;

	public GUIStyle GUIStyleMessageBoxContent;

	public GUIStyle GUIStyleScreenMessageTitle;
	public GUIStyle GUIStyleScreenMessageMessage;
	public GUIStyle GUIStyleScreenProgressMessage;

	public GUIStyle GUIStyleTooltipItemDescription;
	public GUIStyle GUIStyleTooltipItemInfo;
	public GUIStyle GUIStyleTooltipItemInfoUse;
	public GUIStyle GUIStyleTooltipItemName;
	public GUIStyle GUIStyleTooltipNpcHostility;
	public GUIStyle GUIStyleTooltipNpcName;
	public GUIStyle GUIStyleTooltipInfo;

	public GUIStyle GUIStyleWindow;
	public GUIStyle GUIStyleWindowInteractionContent;
	public GUIStyle GUIStyleWindowContextMenu;
	public GUIStyle GUIStyleWindowTitle;
	public GUIStyle GUIStyleWindowTooltip;
	public GUIStyle GUIStyleWindowCharacterCreation;

	public GUISkin DefaultSkin;

	public Color ColorGame;
	public Color ColorParty;
	public Color ColorGuild;
	public Color ColorGeneral;
	public Color ColorTell;

	public Color ChannelColorParty;
	public Color ChannelColorGuild;
	public Color ChannelColorGeneral;
	public Color ChannelColorTell;

	public Color GetMessageColor(MessageType messageType)
	{
		switch (messageType)
		{
			case MessageType.Local:
			case MessageType.Trade:
				return ColorGeneral;

			case MessageType.Guild:
				return ColorGuild;

			case MessageType.Group:
				return ColorParty;

			case MessageType.Game:
				return ColorGame;

			case MessageType.Tell:
				return ColorTell;
		}

		return Color.black;
	}

	public Color GetChannelMessageColor(MessageType messageType)
	{
		switch (messageType)
		{
			case MessageType.Local:
			case MessageType.Trade:
				return ChannelColorGeneral;

			case MessageType.Guild:
				return ChannelColorGuild;

			case MessageType.Group:
				return ChannelColorParty;

			case MessageType.Tell:
				return ChannelColorTell;
		}

		return Color.black;
	}
};