﻿using UnityEngine;

using Karen90MmoFramework.Client.Game;

namespace Karen90MmoFramework.Hud
{
	public interface IDraggable
	{
		/// <summary>
		/// Gets the item id
		/// </summary>
		short ItemId { get; }
		
		/// <summary>
		/// Gets the drag texture
		/// </summary>
		Texture2D Icon { get; }

		/// <summary>
		/// Gets the item type
		/// </summary>
		DragItemType DragItemType { get; }
	}
}
