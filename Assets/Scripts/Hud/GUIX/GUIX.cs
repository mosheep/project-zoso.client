﻿using UnityEngine;

using System;
using System.Globalization;
using System.Collections.Generic;

namespace Karen90MmoFramework.Hud
{
	public static class GUIX
	{
		private static Color? _fadableGroupColor;
		private static Color? _colorFieldColor;

		private static readonly Dictionary<int, string> _numberCache = new Dictionary<int, string>(200);

		private static readonly GUIContent _textContent = new GUIContent();
		private static readonly GUIContent _imageContent = new GUIContent();
		private static readonly GUIContent _winContent = new GUIContent();

		static GUIX()
		{
			for (int i = 0; i < 100; i++)
			{
				_numberCache.Add(i, i.ToString());
			}
		}
		
		/// <summary>
		/// Creates a context menu with buttons
		/// </summary>
		public static int ContextMenu(Rect rect, ref bool open, string[] menuItems, GUIStyle popupStyle, GUIStyle buttonStyle)
		{
			GUI.BeginGroup(rect, popupStyle);
			{
				for (int i = 0; i < menuItems.Length; i++)
				{
					Rect rectBtn = new Rect(3, i * 22 + 3, rect.width - 6, 20);

					var item = menuItems[i];
					if (GUIX.Button(rectBtn, item, buttonStyle))
					{
						open = false;
						return i;
					}
				}
			}
			GUI.EndGroup();

			return -1;
		}

		/// <summary>
		/// Creates a context menu with buttons
		/// </summary>
		public static void ContextMenu(Rect rect, ref bool open, KeyValuePair<string, Action>[] menuItems, GUIStyle popupStyle, GUIStyle buttonStyle)
		{
			GUI.BeginGroup(rect, popupStyle);
			{
				for (int i = 0; i < menuItems.Length; i++)
				{
					Rect rectBtn = new Rect(3, i * 22 + 3, rect.width - 6, 20);

					var item = menuItems[i];
					if (GUIX.Button(rectBtn, item.Key, buttonStyle))
					{
						item.Value();
						open = false;
					}
				}
			}
			GUI.EndGroup();
		}

		/// <summary>
		/// Creates a label with an outline with an offset which defines how far the outline stretches from the label
		/// </summary>
		public static void LabelOutlineHard(Rect rect, string content, float offset, GUIStyle style)
		{
			rect.x += offset;
			Color color = style.normal.textColor;
			style.normal.textColor = new Color(0, 0, 0);
			GUI.Label(rect, content, style);

			rect.y += offset;
			GUI.Label(rect, content, style);

			rect.x -= offset * 2;
			GUI.Label(rect, content, style);

			rect.y -= offset * 2;
			GUI.Label(rect, content, style);

			rect.x += offset;
			rect.y += offset;
			style.normal.textColor = color;
			GUI.Label(rect, content, style);
		}

		/// <summary>
		/// Creates a label with an outline with an offset which defines how far the outline stretches from the label
		/// </summary>
		public static void LabelOutlineHard(Rect rect, string content, GUIStyle style)
		{
			LabelOutlineHard(rect, content, 1, style);
		}

		/// <summary>
		/// Creates a label with an outline with an offset which defines the thickness of the outline and an alpha for defining the percent of visiblity from 0-1
		/// </summary>
		public static void LabelOutline(Rect rect, string content, float offset, float alpha, GUIStyle style)
		{
			rect.x += offset;
			rect.y += offset;
			Color color = style.normal.textColor;
			color.a = alpha;
			style.normal.textColor = new Color(0, 0, 0, alpha);
			GUI.Label(rect, content, style);

			rect.x -= offset;
			rect.y -= offset;
			style.normal.textColor = color;
			GUI.Label(rect, content, style);
		}

		/// <summary>
		/// Creates a label with an outline with an offset which defines how far the outline stretches from the label
		/// </summary>
		public static void LabelOutline(Rect rect, string content, float offset, GUIStyle style)
		{
			rect.x += offset;
			rect.y += offset;
			Color color = style.normal.textColor;
			style.normal.textColor = new Color(0, 0, 0);
			GUI.Label(rect, content, style);

			rect.x -= offset;
			rect.y -= offset;
			style.normal.textColor = color;
			GUI.Label(rect, content, style);
		}

		/// <summary>
		/// Creates a label with an outline with a default outline width of 1
		/// </summary>
		public static void LabelOutline(Rect rect, string content, GUIStyle style)
		{
			LabelOutline(rect, content, 1, style);
		}

		/// <summary>
		/// Creates a label with an outline with a default outline width of 1
		/// </summary>
		public static void LabelOutline(Rect rect, GUIContent content, GUIStyle style)
		{
			rect.x++;
			rect.y++;
			Color color = style.normal.textColor;
			style.normal.textColor = new Color(0, 0, 0);
			GUI.Label(rect, content, style);

			rect.x--;
			rect.y--;
			style.normal.textColor = color;
			GUI.Label(rect, content, style);
		}

		/// <summary>
		/// Componets drawn after this function call will have their alpha value set to the one provided. Must be followed by EndFadableGroup
		/// </summary>
		/// <param name="alpha"></param>
		public static void BeginFadableGroup(float alpha = 1.0f)
		{
			if (_fadableGroupColor != null)
				throw new UnityException("EndFadableGroup must be called");

			Color tempColor = GUI.color;
			_fadableGroupColor = tempColor;

			tempColor.a = alpha;
			GUI.color = tempColor;
		}

		/// <summary>
		/// Ends the fadable group
		/// </summary>
		public static void EndFadableGroup()
		{
			if (!_fadableGroupColor.HasValue)
				throw new UnityException("BeginFadableGroup must be called");

			GUI.color = _fadableGroupColor.Value;

			_fadableGroupColor = null;
		}

		/// <summary>
		/// Componets drawn after this function call will have their font color set to the one provided. Must be followed by EndColorField
		/// </summary>
		/// <param name="color"></param>
		public static void BeginColorField(Color color)
		{
			if (Event.current.type == EventType.Repaint)
			{
				if (_colorFieldColor != null)
				{
					throw new UnityException("EndColorField must be called");
				}

				_colorFieldColor = GUI.color;
				GUI.color = color;
			}
		}

		/// <summary>
		/// Ends the color field
		/// </summary>
		public static void EndColorField()
		{
			if (Event.current.type == EventType.Repaint)
			{
				if (!_colorFieldColor.HasValue)
				{
					throw new UnityException("BeginColorField must be called");
				}

				GUI.color = _colorFieldColor.Value;
				_colorFieldColor = null;
			}
		}

		/// <summary>
		/// Creates a numeric label which uses a lookup table to print a number instead of invoking the ToString() method which is costly
		/// </summary>
		public static void NumLabel(Rect rect, int number, GUIStyle style)
		{
			string num;
			if (!_numberCache.TryGetValue(number, out num))
			{
				if (_numberCache.Count > 1000)
				{
					Logger.Debug("Clearing number cache");
					_numberCache.Clear();
				}

				num = number.ToString(CultureInfo.InvariantCulture);
				_numberCache.Add(number, num);
			}

			GUI.Label(rect, num, style);
		}

		/// <summary>
		/// Creates a numeric label which uses a lookup table to print a number instead of invoking the ToString() method which is costly
		/// </summary>
		public static void NumLabel(Rect rect, int number)
		{
			string num;
			if (!_numberCache.TryGetValue(number, out num))
			{
				if (_numberCache.Count > 1000)
				{
					Logger.Debug("Clearing number cache");
					_numberCache.Clear();
				}

				num = number.ToString();
				_numberCache.Add(number, num);
			}

			GUI.Label(rect, num);
		}

		/// <summary>
		/// Draws a tooltip label
		/// </summary>
		public static void TooltipLabel(Rect rect, GUIContent content)
		{
			TooltipLabel(rect, content, content.tooltip);
		}

		/// <summary>
		/// Draws a tooltip label
		/// </summary>
		public static void TooltipLabel(Rect rect, GUIContent content, GUIStyle guiStyle)
		{
			TooltipLabel(rect, content, content.tooltip, guiStyle);
		}

		/// <summary>
		/// Draws a tooltip label
		/// </summary>
		public static void TooltipLabel(Rect rect, GUIContent content, string tooltip)
		{
			TooltipLabel(rect, content, tooltip, GUI.skin.FindStyle("label"));
		}

		/// <summary>
		/// Draws a tooltip label
		/// </summary>
		public static void TooltipLabel(Rect rect, GUIContent content, string tooltip, GUIStyle guiStyle)
		{
			var controlId = GUIUtility.GetControlID(FocusType.Passive);
			switch (Event.current.type)
			{
				case EventType.Repaint:
					{
						if (rect.Contains(Event.current.mousePosition))
						{
							if (TooltipManager.tooltipControl != controlId)
							{
								TooltipManager.SetTooltip(rect, tooltip);
								TooltipManager.tooltipControl = controlId;
							}

							TooltipManager.ValidateTooltip();
						}
						else
						{
							if (TooltipManager.tooltipControl == controlId)
								TooltipManager.tooltipControl = 0;
						}

						//content.tooltip = null;
						guiStyle.Draw(rect, content, controlId);
					}
					break;
			}
		}

		/// <summary>
		/// Draws a tooltip label
		/// </summary>
		public static void TooltipLabel(Rect rect, Vector2 offset, GUIContent content)
		{
			TooltipLabel(rect, offset, content, content.tooltip);
		}

		/// <summary>
		/// Draws a tooltip label
		/// </summary>
		public static void TooltipLabel(Rect rect, Vector2 offset, GUIContent content, GUIStyle guiStyle)
		{
			TooltipLabel(rect, offset, content, content.tooltip, guiStyle);
		}

		/// <summary>
		/// Draws a tooltip label
		/// </summary>
		public static void TooltipLabel(Rect rect, Vector2 offset, GUIContent content, string tooltip)
		{
			TooltipLabel(rect, offset, content, tooltip, GUI.skin.FindStyle("label"));
		}

		/// <summary>
		/// Draws a tooltip label
		/// </summary>
		public static void TooltipLabel(Rect rect, Vector2 offset, GUIContent content, string tooltip, GUIStyle guiStyle)
		{
			var controlId = GUIUtility.GetControlID(FocusType.Passive);
			switch (Event.current.type)
			{
				case EventType.Repaint:
					{
						if (rect.Contains(Event.current.mousePosition))
						{
							if (TooltipManager.tooltipControl != controlId)
							{
								TooltipManager.SetTooltip(rect, offset, tooltip);
								TooltipManager.tooltipControl = controlId;
							}

							TooltipManager.ValidateTooltip();
						}
						else
						{
							if (TooltipManager.tooltipControl == controlId)
								TooltipManager.tooltipControl = 0;
						}

						//content.tooltip = null;
						guiStyle.Draw(rect, content, controlId);
					}
					break;
			}
		}

		/// <summary>
		/// Draws a tooltip button
		/// </summary>
		public static bool TooltipButton(Rect rect, GUIContent content)
		{
			return TooltipButton(rect, content, content.tooltip);
		}

		/// <summary>
		/// Draws a tooltip button
		/// </summary>
		public static bool TooltipButton(Rect rect, GUIContent content, GUIStyle guiStyle)
		{
			return TooltipButton(rect, content, content.tooltip, guiStyle);
		}

		/// <summary>
		/// Draws a tooltip button
		/// </summary>
		public static bool TooltipButton(Rect rect, GUIContent content, string tooltip)
		{
			return TooltipButton(rect, content, tooltip, GUI.skin.FindStyle("button"));
		}

		/// <summary>
		/// Draws a tooltip button
		/// </summary>
		public static bool TooltipButton(Rect rect, GUIContent content, string tooltip, GUIStyle guiStyle)
		{
			var controlId = GUIUtility.GetControlID(FocusType.Native);
			switch (Event.current.type)
			{
				case EventType.Repaint:
					{
						if (rect.Contains(Event.current.mousePosition))
						{
							if (TooltipManager.tooltipControl != controlId)
							{
								TooltipManager.SetTooltip(rect, tooltip);
								TooltipManager.tooltipControl = controlId;
							}

							TooltipManager.ValidateTooltip();
						}
						else
						{
							if (TooltipManager.tooltipControl == controlId)
								TooltipManager.tooltipControl = 0;
						}
					}
					break;
			}

			return Button(rect, content, guiStyle);
		}

		/// <summary>
		/// Draws a tooltip button
		/// </summary>
		public static bool TooltipButton(Rect rect, Vector2 offset, GUIContent content)
		{
			return TooltipButton(rect, offset, content, content.tooltip);
		}

		/// <summary>
		/// Draws a tooltip button
		/// </summary>
		public static bool TooltipButton(Rect rect, Vector2 offset, GUIContent content, GUIStyle guiStyle)
		{
			return TooltipButton(rect, offset, content, content.tooltip, guiStyle);
		}

		/// <summary>
		/// Draws a tooltip button
		/// </summary>
		public static bool TooltipButton(Rect rect, Vector2 offset, GUIContent content, string tooltip)
		{
			return TooltipButton(rect, offset, content, tooltip, GUI.skin.FindStyle("button"));
		}

		/// <summary>
		/// Draws a tooltip button
		/// </summary>
		public static bool TooltipButton(Rect rect, Vector2 offset, GUIContent content, string tooltip, GUIStyle guiStyle)
		{
			var controlId = GUIUtility.GetControlID(FocusType.Native);
			switch (Event.current.type)
			{
				case EventType.Repaint:
					{
						if (rect.Contains(Event.current.mousePosition))
						{
							if (TooltipManager.tooltipControl != controlId)
							{
								TooltipManager.SetTooltip(rect, offset, tooltip);
								TooltipManager.tooltipControl = controlId;
							}

							TooltipManager.ValidateTooltip();
						}
						else
						{
							if (TooltipManager.tooltipControl == controlId)
								TooltipManager.tooltipControl = 0;
						}
					}
					break;
			}

			return Button(rect, content, guiStyle);
		}

		/// <summary>
		/// This button gives precedence to controls drawn on top of this
		/// </summary>
		public static bool Button(Rect rect, string content)
		{
			_textContent.text = content;
			return Button(rect, _textContent);
		}

		/// <summary>
		/// This button gives precedence to controls drawn on top of this
		/// </summary>
		public static bool Button(Rect rect, string content, GUIStyle guiStyle)
		{
			_textContent.text = content;
			return Button(rect, _textContent, guiStyle);
		}

		/// <summary>
		/// This button gives precedence to controls drawn on top of this
		/// </summary>
		public static bool Button(Rect rect, Texture2D content)
		{
			_imageContent.image = content;
			return Button(rect, _imageContent);
		}

		/// <summary>
		/// This button gives precedence to controls drawn on top of this
		/// </summary>
		public static bool Button(Rect rect, Texture2D content, GUIStyle guiStyle)
		{
			_imageContent.image = content;
			return Button(rect, _imageContent, guiStyle);
		}

		/// <summary>
		/// This button gives precedence to controls drawn on top of this
		/// </summary>
		public static bool Button(Rect rect, GUIContent content)
		{
			return Button(rect, content, GUI.skin.FindStyle("button"));
		}

		/// <summary>
		/// Draws a button which works in sync with GUIX.Window
		/// </summary>
		public static bool Button(Rect rect, GUIContent content, GUIStyle guiStyle)
		{
			var controlId = GUIUtility.GetControlID(FocusType.Native);
			switch (Event.current.type)
			{
				case EventType.Repaint:
					{
						guiStyle.Draw(rect, content, controlId);
					}
					break;

				case EventType.MouseDown:
					{
						if (rect.Contains(Event.current.mousePosition))
						{
							if(GUIUtility.hotControl == 0)
								GUIUtility.hotControl = controlId;
						}
					}
					break;
					
				case EventType.MouseUp:
					{
						if (GUIUtility.hotControl == controlId && rect.Contains(Event.current.mousePosition))
						{
							GUIUtility.hotControl = 0;
							return true;
						}
					}
					break;
			}

			return false;
		}

		/// <summary>
		/// A button which allows expansion on mouse hover
		/// </summary>
		public static bool ExpansionButton(Rect rect, Texture2D texture, int expansion)
		{
			_imageContent.image = texture;
			return ExpansionButton(rect, _imageContent, expansion, GUI.skin.FindStyle("button"));
		}

		/// <summary>
		/// A button which allows expansion on mouse hover
		/// </summary>
		public static bool ExpansionButton(Rect rect, Texture2D texture, int expansion, GUIStyle guiStyle)
		{
			_imageContent.image = texture;
			return ExpansionButton(rect, _imageContent, expansion, guiStyle);
		}

		/// <summary>
		/// A button which allows expansion on mouse hover
		/// </summary>
		public static bool ExpansionButton(Rect rect, GUIContent content, int expansion, GUIStyle guiStyle)
		{
			if(Event.current.type == EventType.Repaint)
			{
				if (rect.Contains(Event.current.mousePosition))
				{
					return Button(new Rect(rect.x - expansion, rect.y - expansion, rect.width + expansion * 2, rect.height + expansion * 2),
					              content, guiStyle);
				}
			}
			
			return Button(rect, content, guiStyle);
		}

		/// <summary>
		/// This button gives precedence to controls drawn on top of this
		/// </summary>
		public static bool SelectButton(Rect rect, bool selected, string content)
		{
			_textContent.text = content;
			return SelectButton(rect, selected, _textContent);
		}

		/// <summary>
		/// This button gives precedence to controls drawn on top of this
		/// </summary>
		public static bool SelectButton(Rect rect, bool selected, string content, GUIStyle guiStyle)
		{
			_textContent.text = content;
			return SelectButton(rect, selected, _textContent, guiStyle);
		}

		/// <summary>
		/// This button gives precedence to controls drawn on top of this
		/// </summary>
		public static bool SelectButton(Rect rect, bool selected, Texture2D content)
		{
			_imageContent.image = content;
			return SelectButton(rect, selected, _imageContent);
		}

		/// <summary>
		/// This button gives precedence to controls drawn on top of this
		/// </summary>
		public static bool SelectButton(Rect rect, bool selected, Texture2D content, GUIStyle guiStyle)
		{
			_imageContent.image = content;
			return SelectButton(rect, selected, _imageContent, guiStyle);
		}

		/// <summary>
		/// This button gives precedence to controls drawn on top of this
		/// </summary>
		public static bool SelectButton(Rect rect, bool selected, GUIContent content)
		{
			return SelectButton(rect, selected, content, GUI.skin.FindStyle("button"));
		}

		/// <summary>
		/// Draws a button which works in sync with GUIX.Window
		/// </summary>
		public static bool SelectButton(Rect rect, bool selected, GUIContent content, GUIStyle guiStyle)
		{
			var controlId = GUIUtility.GetControlID(FocusType.Native);
			switch (Event.current.type)
			{
				case EventType.Repaint:
					{
						if (!selected)
						{
							guiStyle.Draw(rect, content, controlId);
						}
						else
						{
							guiStyle.Draw(rect, content, controlId, true);
						}
					}
					break;

				case EventType.MouseDown:
					{
						if (rect.Contains(Event.current.mousePosition))
						{
							if (GUIUtility.hotControl == 0)
								GUIUtility.hotControl = controlId;
						}
					}
					break;

				case EventType.MouseUp:
					{
						if (GUIUtility.hotControl == controlId && rect.Contains(Event.current.mousePosition))
						{
							GUIUtility.hotControl = 0;
							return true;
						}
					}
					break;
			}

			return false;
		}

		/// <summary>
		/// Draws a window which gets precedence to its controls first
		/// </summary>
		public static void Window(int id, Rect rect, Texture2D content, GUIStyle guiStyle)
		{
			var controlId = GUIUtility.GetControlID(FocusType.Native);
			switch (HUD.eventType)
			{
				case EventType.Repaint:
					{
						_winContent.image = content;
						guiStyle.Draw(rect, _winContent, controlId);
					}
					break;

				case EventType.ScrollWheel:
					{
						if (rect.Contains(Event.current.mousePosition))
							HUD.scrollControl = controlId;
					}
					break;

				case EventType.MouseDown:
					{
						if (rect.Contains(Event.current.mousePosition))
						{
							GUIUtility.hotControl = 0;
							HUD.winControl = id;
						}
					}
					break;
			}
		}
	}
}
