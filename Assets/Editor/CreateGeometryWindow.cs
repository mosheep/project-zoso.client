﻿using UnityEngine;
using UnityEditor;

public class CreateGeometryWindow : EditorWindow
{
	enum MeshType
	{
		Quad,
	}

	[MenuItem("&Utils/Create Geometry...")]
	static void Init()
	{
		GetWindow(typeof(CreateGeometryWindow));
	}

	private Vector3 pivot;
	private MeshType meshType;
	private Vector2 size;
	private bool face;

	void Awake()
	{
		this.title = "Create Geometry";
		this.pivot = Vector3.zero;
		this.size = new Vector2(1, 1);
	}

	void OnGUI()
	{
		GUILayout.Label("Pivot:");
		GUILayout.Space(5);
		EditorGUILayout.BeginHorizontal();
		{
			GUILayout.Space(10);
			GUILayout.Label("X");
			GUILayout.Space(10);
			pivot.x = EditorGUILayout.Slider(pivot.x, -1, 1);
		}
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		{
			GUILayout.Space(10);
			GUILayout.Label("Y");
			GUILayout.Space(10);
			pivot.y = EditorGUILayout.Slider(pivot.y, -1, 1);
		}
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		{
			GUILayout.Space(10);
			GUILayout.Label("Z");
			GUILayout.Space(10);
			pivot.z = EditorGUILayout.Slider(pivot.z, -1, 1);
		}
		EditorGUILayout.EndHorizontal();

		GUILayout.Space(10);
		EditorGUILayout.BeginHorizontal();
		{
			GUILayout.Label("Mesh:");
			meshType = (MeshType)EditorGUILayout.EnumPopup(meshType);
			GUILayout.FlexibleSpace();
		}
		EditorGUILayout.EndHorizontal();

		GUILayout.Space(10);
		EditorGUILayout.BeginHorizontal();
		{
			GUILayout.Label("Direction:");
			face = GUILayout.Toggle(face, "Forward");
			GUILayout.FlexibleSpace();
		}
		EditorGUILayout.EndHorizontal();
		
		switch (meshType)
		{
			case MeshType.Quad:
			{
				size = EditorGUILayout.Vector2Field("Size:", size);
			}
			break;
		}

		if (GUI.Button(new Rect(position.width - 110, position.height - 30, 100, 20), "Create"))
		{
			CreateLowPolyPlane(pivot, size, face);
			this.Close();
		}
	}

	private static void CreateLowPolyPlane(Vector3 pivot, Vector2 size, bool face)
	{
		if(size.x <= 0 || size.y <= 0)
		{
			Debug.LogError("Invalid Size");
			return;
		}

		var mesh = CreatePlane(pivot, size, face);

		EditorHelper.CreateAssetFolderIfNotExists("Prefabs/Mesh");

		var path = AssetDatabase.GenerateUniqueAssetPath(string.Format("Assets/Prefabs/Mesh/{0}.obj", mesh.name));
		AssetDatabase.CreateAsset(mesh, string.Format(path, mesh.name));

		Debug.Log(string.Format("mesh {0} created automatically under '{1}'", mesh.name, path));

		var go = new GameObject("low-poly-plane");

		go.AddComponent<MeshFilter>();
		go.AddComponent<MeshRenderer>();

		go.GetComponent<MeshFilter>().sharedMesh = mesh;
	}

	private static Mesh CreatePlane( Vector3 pivot, Vector2 size, bool face)
	{
		var center = new Vector2(size.x/2, size.y/2);

		pivot.x = (pivot.x - 1) * center.x;
		pivot.y = (pivot.y - 1) * center.y;
		pivot.z = 0;

		Mesh mesh;
		if (!face)
		{
			mesh = new Mesh
				{
					name = "low-poly-plane",
					vertices =
						new[]
							{
								new Vector3(pivot.x, pivot.y, pivot.z), new Vector3(pivot.x + size.x, pivot.y, pivot.z),
								new Vector3(pivot.x, pivot.y + size.y, pivot.z),
								new Vector3(pivot.x + size.x, pivot.y + size.y, pivot.z),
							},
					uv =
						new[] { new Vector2(0, 0), new Vector2(1, 0), new Vector2(0, 1), new Vector2(1, 1) },
					triangles = new int[] { 0, 2, 1, 2, 3, 1 }
				};
		}
		else
		{
			mesh = new Mesh
				{
					name = "low-poly-plane",
					vertices =
						new[]
							{
								new Vector3(pivot.x, pivot.y, pivot.z),
								new Vector3(pivot.x + size.x, pivot.y, pivot.z),
								new Vector3(pivot.x, pivot.y + size.y, pivot.z),
								new Vector3(pivot.x + size.x, pivot.y + size.y, pivot.z),
							},
					uv =
						new[] {new Vector2(1, 0), new Vector2(0, 0), new Vector2(1, 1), new Vector2(0, 1)},
					triangles = new int[] {0, 1, 2, 1, 3, 2}
				};
		}

		mesh.RecalculateBounds();
		mesh.RecalculateNormals();

		return mesh;
	}
}