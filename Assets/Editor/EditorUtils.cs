using System;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using Object = UnityEngine.Object;
using Vector3 = UnityEngine.Vector3;

public class EditorUtils : MonoBehaviour
{
	[MenuItem("&Utils/Create Minimap Planes")]
	private static void CreateMapPlanes()
	{
		var settingsAsset = AssetDatabase.LoadAssetAtPath("Assets/Minimap/settings.txt", typeof(TextAsset)) as TextAsset;
		if (settingsAsset == null)
		{
			Debug.LogError("Settings file not found under Minimap folder");
			return;
		}

		var mSettings = new MapSettings(settingsAsset.text);

		var length = mSettings.length;
		var width = mSettings.width;

		var mesh = CreatePlane(length, width);

		EditorHelper.CreateAssetFolderIfNotExists("Minimap/Mesh");
		AssetDatabase.CreateAsset(mesh, "Assets/Minimap/Mesh/plane.obj");

		var selection = Selection.GetFiltered(typeof(Texture), SelectionMode.DeepAssets);
		if (selection != null)
		{
			for (int i = 0; i < selection.Length; i++)
			{
				var texture = selection[i] as Texture;
				if (texture != null)
				{
					var mat = CreateMaterial(texture, texture.name);
					var prefab = CreatePrefab(mesh, texture.name);
					prefab.GetComponent<Renderer>().material = mat;
				}
			}
		}
	}

	[MenuItem("&Utils/Create Minimap")]
	private static void Test()
	{
		Debug.Log(Resources.Load("Cursors/cursor_default"));
	}

	[MenuItem("&Utils/Export Selection To Map Data...")]
	private static void ExportMapData()
	{
		var path = EditorUtility.SaveFilePanel("Save Asset", "", "New Data", "dat");
		if (string.IsNullOrEmpty(path) == false)
		{
			var settingsAsset = AssetDatabase.LoadAssetAtPath("Assets/Minimap/settings.txt", typeof(TextAsset));
			if (settingsAsset == null)
			{
				Debug.LogError("Settings file not found under Minimap folder");
				return;
			}

			var selection = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
			BuildPipeline.BuildAssetBundle(settingsAsset, selection, path, BuildAssetBundleOptions.UncompressedAssetBundle | BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets, BuildTarget.StandaloneWindows);
			Selection.objects = selection;
		}
	}

	private static Mesh CreatePlane(int length, int width)
	{
		var mesh = new Mesh
			{
				name = "low-poly-plane",
				vertices =
					new Vector3[]
						{
							new Vector3(0, 0, 0), new Vector3(0, width, 0), new Vector3(length, width, 0), new Vector3(length, width, 0),
							new Vector3(length, 0, 0), new Vector3(0, 0, 0)
						},
				uv =
					new Vector2[]
						{new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(0, 0)},
				triangles = new int[] {0, 1, 2, 3, 4, 5}
			};

		mesh.RecalculateNormals();

		return mesh;
	}

	private static Material CreateMaterial(Texture texture, string name)
	{
		var material = new Material(Shader.Find("Unlit/Texture"));
		material.mainTexture = texture;
		material.name = name;

		EditorHelper.CreateAssetFolderIfNotExists("Minimap/Prefabs/Materials");
		AssetDatabase.CreateAsset(material, "Assets/Minimap/Prefabs/Materials/" + name + ".mat");

		return material;
	}

	private static GameObject CreatePrefab(Mesh mesh, string name)
	{
		var go = new GameObject(name);
		go.AddComponent<MeshFilter>();
		go.AddComponent<MeshRenderer>();
		go.transform.Rotate(new Vector3(1, 0, 0), 90);

		EditorHelper.CreateAssetFolderIfNotExists("Minimap/Prefabs");
		var prefab = PrefabUtility.CreatePrefab("Assets/Minimap/Prefabs/" + name + ".prefab", go);

		prefab.GetComponent<MeshFilter>().sharedMesh = mesh;
		prefab.GetComponent<MeshRenderer>().castShadows = false;
		prefab.GetComponent<MeshRenderer>().receiveShadows = false;

		return prefab;
	}

	[MenuItem("&Utils/Export Terrain...")]
	private static void ExportTerrain()
	{
		var selection = Selection.activeGameObject;
		if(selection == null)
		{
			Debug.LogError("Need a selection");
			return;
		}
		
		var selectedTerrain = selection.GetComponent<Terrain>();
		if (selectedTerrain == null)
		{
			Debug.LogError("Selection has to be a terrain");
			return;
		}

		var terrainData = selectedTerrain.terrainData;

		var width = (int)terrainData.size.x;
		var length = (int)terrainData.size.z;

		var path = EditorUtility.SaveFilePanel("Save Map", "", "New Map", "map");
		if(string.IsNullOrEmpty(path))
			return;

		using (var fstream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite))
		{
			// writing the width
			var buffer = BitConverter.GetBytes(width);
			fstream.Write(buffer, 0, buffer.Length);

			// writing the length
			buffer = BitConverter.GetBytes(length);
			fstream.Write(buffer, 0, buffer.Length);

			for (var z = 0; z < length; z++)
			{
				for (var x = 0; x < width; x++)
				{
					// this offset only applies to our map only
					// since the map was flattened 200 above the 0 level
					var height = selectedTerrain.SampleHeight(new Vector3(x, 0, z)) - 200;

					// writing the heights
					buffer = BitConverter.GetBytes(height);
					fstream.Write(buffer, 0, buffer.Length);
				}
			}
		}
	}

	[MenuItem("&Utils/Export Colliders...")]
	private static void ExportColliders()
	{
		var path = EditorUtility.SaveFilePanel("Export Colliders", "", "map.colliders", "xml");
		if (string.IsNullOrEmpty(path))
			return;

		var boxColliders = new List<Karen90MmoFramework.Quantum.Geometry.BoxDescription>();
		var sphereColliders = new List<Karen90MmoFramework.Quantum.Geometry.SphereDescription>();
		var capsuleColliders = new List<Karen90MmoFramework.Quantum.Geometry.CapsuleDescription>();

		var colliders = FindObjectsOfType<Collider>();
		foreach (var collider in colliders)
		{
			if(collider.isTrigger)
				continue;

			if(!collider.enabled)
				continue;

			if (!collider.transform.gameObject.activeSelf)
				continue;

			var boxCollider = collider as BoxCollider;
			if (boxCollider != null)
			{
				var center = boxCollider.center;
				var position = boxCollider.transform.position + center;
				var rotation = boxCollider.transform.rotation;
				var scale = boxCollider.transform.localScale;
				var size = boxCollider.size;

				var box = new Karen90MmoFramework.Quantum.Geometry.BoxDescription
				          {
					          Position = new Karen90MmoFramework.Quantum.Vector3(position.x, position.y, position.z),
					          Rotation = new Karen90MmoFramework.Quantum.Quaternion(rotation.x, rotation.y, rotation.z, rotation.w),
					          Scale = new Karen90MmoFramework.Quantum.Vector3(scale.x, scale.y, scale.z),
					          Size = new Karen90MmoFramework.Quantum.Vector3(size.x, size.y, size.z)
				          };
				boxColliders.Add(box);

				continue;
			}

			var sphereCollider = collider as SphereCollider;
			if (sphereCollider != null)
			{
				var center = sphereCollider.center;
				var position = sphereCollider.transform.position + center;
				var rotation = sphereCollider.transform.rotation;
				var scale = sphereCollider.transform.localScale;
				var radius = sphereCollider.radius;

				var sphere = new Karen90MmoFramework.Quantum.Geometry.SphereDescription()
					{
						Position = new Karen90MmoFramework.Quantum.Vector3(position.x, position.y, position.z),
						Rotation = new Karen90MmoFramework.Quantum.Quaternion(rotation.x, rotation.y, rotation.z, rotation.w),
						Scale = new Karen90MmoFramework.Quantum.Vector3(scale.x, scale.y, scale.z),
						Radius = radius,
					};
				sphereColliders.Add(sphere);

				continue;
			}

			var capsuleCollider = collider as CapsuleCollider;
			if (capsuleCollider != null)
			{
				var center = capsuleCollider.center;
				var position = capsuleCollider.transform.position + center;
				var rotation = capsuleCollider.transform.rotation;
				var scale = capsuleCollider.transform.localScale;
				var radius = capsuleCollider.radius;
				var height = capsuleCollider.height;

				var capsule = new Karen90MmoFramework.Quantum.Geometry.CapsuleDescription()
					{
						Position = new Karen90MmoFramework.Quantum.Vector3(position.x, position.y, position.z),
						Rotation = new Karen90MmoFramework.Quantum.Quaternion(rotation.x, rotation.y, rotation.z, rotation.w),
						Scale = new Karen90MmoFramework.Quantum.Vector3(scale.x, scale.y, scale.z),
						Radius = radius,
						Height = height
					};
				capsuleColliders.Add(capsule);
			}
		}

		var collidersData = new Karen90MmoFramework.Colliders()
			{
				BoxColliders = boxColliders.ToArray(),
				CapsuleColliders = capsuleColliders.ToArray(),
				SphereColliders = sphereColliders.ToArray()
			};

		var writer = new StreamWriter(path);
		var serializer = new XmlSerializer(typeof(Karen90MmoFramework.Colliders));
		serializer.Serialize(writer, collidersData);
		writer.Close();
	}
}
